# Hex: Projet s6 de C

## Developers :

- BENARD Baptiste
- CANET Ivan
- COMET Raphaël
- MOENNE-LOCCOZ Tom

## Options

Ce project dépend d'une installation valide de Git, Make, GCC, CMake, LuaLaTeX, Valgrind.

Pour lancer les tests automatiques:
- `make test`

Pour compiler le serveur et les stratégies:
- `make install`

Pour lancer le serveur:
- `./install/server [-m TAILLE] [-t FORME] <stratégie1.so> <stratégie2.so>`

Nos stratégies sont disponibles sous la forme de fichiers .so dans le dossier 'install'.

Pour compiler le rapport en LaTeX:
- `make report`

Le PDF est généré dans le dossier 'report'. Si vous préférez un autre compilateur que LuaLaTeX, il est possible de spécifier votre compilateur avec la variable d'environement `LATEX_ENGINE`; par exemple pour utiliser PDFLaTeX:
- `LATEX_ENGINE=pdflatex make report`

Si la compilation ne fonctionne pas pour une autre raison, la version la plus récente du rapport est [disponible ici](https://clovis-ai.gitlab.io/enseirb-s6-hex/report.pdf).

Pour récupérer la documentation:

- `make docs-html docs-latex`

La documentation est alors disponible dans `docs/html` et `docs/latex`. Elle est aussi disponible sur [GitLab](https://clovis-ai.gitlab.io/enseirb-s6-hex/index.html).

Pour nettoyer le dépôt:
- `make clean`
