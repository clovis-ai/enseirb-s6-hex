FROM archlinux:latest

RUN pacman -Sy --noconfirm \
	make cmake gcc gsl valgrind glibc libffi doxygen graphviz
