
LATEX_BUILDER="./report/templates/build"
LATEX_FILES="report/report_hex_9154.tex"
LATEX_FLAGS="--no-print-logs"

all: build install test

cmake-build:
	mkdir -p cmake-build
	cd cmake-build && cmake $${GSL_PATH+"-DGSL_PATH=${GSL_PATH}"} .. -DCMAKE_BUILD_TYPE=Debug

.PHONY: build
build: cmake-build
	@$(MAKE) -s -C cmake-build server random alltests

.PHONY: test
test: install
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes install/alltests

.PHONY: install
install: build
	cp cmake-build/src/common/alltests install/alltests
	cp cmake-build/src/server/server install/server
	cp cmake-build/src/kirchoff/libkirchoff.so install/kirchoff.so
	cp cmake-build/src/line/libline.so install/line.so
	cp cmake-build/src/random/librandom.so install/random.so
	cp cmake-build/src/manual/libzmanual.so install/zmanual.so

.PHONY: clean
clean:
	rm -f  install/*
	rm -rf cmake-build docs
	@([ -f report/templates/build ] && $(LATEX_BUILDER) $(LATEX_FLAGS) HARDCLEAN $(LATEX_FILES)) || true

.PHONY: submodule
submodule:
	git submodule update --init --recursive

.PHONY: report
report: submodule
	@$(LATEX_BUILDER) $(LATEX_FLAGS) PDF $(LATEX_FILES)

.PHONY: docs
docs: doxygen.conf
	doxygen doxygen.conf

docs-html: docs

docs-latex: docs
	$(MAKE) -s -C docs/latex pdf
