#include <stdio.h>
#include "test_suite.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void test_start(char *name) {
	printf("%s \t->\t", name);
}

int test_success() {
	printf(ANSI_COLOR_GREEN "SUCCESS\n" ANSI_COLOR_RESET);
	return 0;
}

int test_failure(char *message) {
	printf(ANSI_COLOR_RED "FAILURE: %s\n" ANSI_COLOR_RESET, message);
	return 1;
}

void test_suite_start(char *name) {
	printf(ANSI_COLOR_BLUE "\nStarting suite: %s\n" ANSI_COLOR_RESET, name);
}
