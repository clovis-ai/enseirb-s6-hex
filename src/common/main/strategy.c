
#include <dlfcn.h>
#include "strategy.h"

struct strategy *load_strategy(const char *name) {
	void *handle = dlopen(name, RTLD_NOW);
	if (!handle)    // Cannot open the strategy
		return 0;

	struct strategy *ret = malloc(sizeof(*ret));
	ret->dl_handle = handle;

	ret->get_player_name = dlsym(handle, "get_player_name");
	ret->propose_opening = dlsym(handle, "propose_opening");
	ret->accept_opening = dlsym(handle, "accept_opening");
	ret->initialize_graph = dlsym(handle, "initialize_graph");
	ret->initialize_color = dlsym(handle, "initialize_color");
	ret->play = dlsym(handle, "play");
	ret->finalize = dlsym(handle, "finalize");

	if (!ret->get_player_name) return 0;
	if (!ret->propose_opening) return 0;
	if (!ret->accept_opening) return 0;
	if (!ret->initialize_graph) return 0;
	if (!ret->initialize_color) return 0;
	if (!ret->play) return 0;
	if (!ret->finalize) return 0;

	return ret;
}

void free_strategy(struct strategy *strategy) {
	dlclose(strategy->dl_handle);
	free(strategy);
}
