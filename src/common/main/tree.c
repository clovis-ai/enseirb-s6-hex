#include "tree.h"
#include <malloc.h>

struct tree_node {
	size_t id;
	unsigned int depth;
	array_t *neighbors;
};

struct tree {
	array_t *nodes;
};

tree_t *tree_new(size_t size) {
	tree_t *tree = malloc(sizeof(*tree));

	tree->nodes = array_new(size);
	for (size_t i = 0; i < size; ++i)
		array_add(tree->nodes, 0);

	return tree;
}

void tree_free(tree_t *tree) {
	for (size_t i = 0; i < array_size(tree->nodes); ++i)
		tree_node_free(array_get(tree->nodes, i));
	array_free(tree->nodes);
	free(tree);
}

void tree_add_node(tree_t *tree, tree_node_t *node) {
	array_set(tree->nodes, tree_node_id(node), node);
}

tree_node_t *tree_get_node(tree_t *tree, size_t id) {
	return array_get(tree->nodes, id);
}

tree_node_t *tree_node_new(size_t id, unsigned int depth) {
	tree_node_t *node = malloc(sizeof(*node));

	node->id = id;
	node->depth = depth;
	node->neighbors = array_new(2);

	return node;
}

void tree_node_free(tree_node_t *node) {
	array_free(node->neighbors);
	free(node);
}

size_t tree_node_id(tree_node_t *node) {
	return node->id;
}

unsigned int tree_node_depth(tree_node_t *node) {
	return node->depth;
}

void tree_node_add_neighbor(tree_node_t *node, tree_node_t *neighbor) {
	array_add(node->neighbors, neighbor);
}

array_t *tree_node_neighbors(tree_node_t *node) {
	return node->neighbors;
}

