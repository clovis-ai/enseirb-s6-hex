#include "kirchhoff_functions.h"

void component_finding(gsl_spmatrix *ownership, gsl_spmatrix *csr, gsl_spmatrix *is_visited, vertex_t s, size_t side,
                       vertex_t *all_vertices) {
	stack_t *stack_todo = stack_new();
	gsl_spmatrix_set(is_visited, 0, s, 1);
	stack_push(stack_todo, all_vertices + s);
	array_t *neighbours;
	vertex_t v;
	while (!stack_is_empty(stack_todo)) {
		s = *((vertex_t *) stack_pop(stack_todo));
		neighbours = get_neighbours(csr, s, all_vertices);
		size_t n = array_size(neighbours);
		for (size_t i = 0; i < n; i++) {
			v = *((vertex_t *) array_get(neighbours, i));
			if (gsl_spmatrix_get(ownership, side, v) == 1 && !gsl_spmatrix_get(is_visited, 0, v)) {
				stack_push(stack_todo, all_vertices + v);
				gsl_spmatrix_set(is_visited, 0, v, 1);
			}
		}
		array_free(neighbours);
	}
	stack_free(stack_todo);

}

void init_single_player_component(const struct graph_t *graph, gsl_spmatrix *csr, vertex_t *s, vertex_t *v, size_t side,
                                  vertex_t *all_vertices) {
	size_t nb_vertices = graph->num_vertices;
	gsl_spmatrix *ownership = graph->o;
	size_t one_found = 0;
	gsl_spmatrix *is_visited = gsl_spmatrix_alloc(1, graph->num_vertices);
	for (size_t i = 0; i < nb_vertices; i++) {
		if (gsl_spmatrix_get(ownership, side, i) && !gsl_spmatrix_get(is_visited, 0, i)) {
			if (!(one_found)) {
				*s = i;
				component_finding(ownership, csr, is_visited, i, side, all_vertices);
				one_found = 1;
			} else {
				*v = i;
				gsl_spmatrix_free(is_visited);
				return;
			}

		}
	}
}

void
init_components(const struct graph_t *graph, gsl_spmatrix *csr, vertex_t *s1, vertex_t *v1, vertex_t *s2, vertex_t *v2,
                vertex_t *all_vertices) {
	init_single_player_component(graph, csr, s1, v1, 0, all_vertices);
	init_single_player_component(graph, csr, s2, v2, 1, all_vertices);
}


double vertex_resist(vertex_t s, const struct graph_t *graph, enum color_t color) {
	if (gsl_spmatrix_get(graph->o, color, s))
		return 0.01;
	if (gsl_spmatrix_get(graph->o, (color ? 0 : 1), s))
		return 100;
	return 1;
}

double edge_resist(vertex_t s, vertex_t v, const struct graph_t *graph, enum color_t color) {
	double r1 = vertex_resist(s, graph, color);
	double r2 = vertex_resist(v, graph, color);
	return r1 + r2;
}


void array_to_cycle(cycle_t *dest, array_t *src, vertex_t *all_vertices) {
	size_t n = array_size(src);
	for (size_t i = 0; i < n; i++)
		cycle_add(dest, *((vertex_t *) array_get(src, i)) + all_vertices);
}


int same_order_cycle(cycle_t *cycle, vertex_t s, vertex_t v) {
	size_t nb_vertices = cycle_size(cycle);
	vertex_t s2;
	vertex_t v2 = *cycle_get(cycle, 0);
	for (size_t i = 0; i < nb_vertices; i++) {
		s2 = v2;
		v2 = *cycle_get(cycle, i + 1);
		if ((s2 == s) && (v2 == v))
			return 1;
		if ((s2 == v) && (v2 == s))
			return 0;
	}
	return -1;
}


array_t *list_edges(const gsl_spmatrix *csr, vertex_t *all_vertices) {
	array_t *E = array_new(1);
	size_t nb_vertices = csr->size1;
	array_t *neighbour;
	vertex_t v;
	edge_t *edge;
	vertex_t *tmp = calloc(6, sizeof(vertex_t));
	size_t size;
	size_t beg;
	vertex_t min;
	size_t index_min = 0;
	for (vertex_t s = 0; s < nb_vertices; s++) {
		neighbour = get_neighbours(csr, s, all_vertices);
		size = array_size(neighbour);
		beg = 0;
		for (size_t i = 0; i < size; i++) {
			v = *((vertex_t *) (array_get(neighbour, i)));
			tmp[i] = v;
		}
		for (size_t i = 0; i < size; i++) {
			min = nb_vertices;
			for (size_t j = beg; j < size; j++) {
				if (min >= tmp[j]) {
					min = tmp[j];
					index_min = j;
				}
			}
			tmp[index_min] = tmp[beg];
			tmp[beg] = min;
			beg++;
		}
		for (size_t i = 0; i < size; i++) {
			v = tmp[i];
			if (s < v) {
				edge = edge_new(all_vertices + s, all_vertices + v);
				array_add(E, edge);
			}
		}
		array_free(neighbour);
	}
	free(tmp);
	return E;
}

void free_list_edges(array_t *all_edges) {
	size_t size_l = array_size(all_edges);
	edge_t *edge;
	for (size_t i = 0; i < size_l; i++) {
		edge = (edge_t *) array_get(all_edges, i);
		edge_free(edge);
	}
	array_free(all_edges);
}


tree_t *spanning_tree(const gsl_spmatrix *csr, size_t s, size_t *used_edges, size_t *all_vertices, array_t *all_edges) {
	size_t nb_vertices = csr->size1;
	size_t *used_vertices = calloc(nb_vertices, sizeof(size_t));
	tree_t *tree = tree_new(nb_vertices);
	queue_t *queue = queue_new();
	used_vertices[s] = 1;
	queue_push(queue, all_vertices + s);
	tree_node_t *tree_node = tree_node_new(s, 0);
	tree_add_node(tree, tree_node);
	vertex_t v;
	array_t *neighbour;
	while (!(queue_is_empty(queue))) {
		s = *((vertex_t *) queue_pop(queue));
		neighbour = get_neighbours(csr, s, all_vertices);
		for (size_t i = 0; i < array_size(neighbour); i++) {
			v = *((vertex_t *) (array_get(neighbour, i)));
			if (used_vertices[v] == 0) {
				queue_push(queue, all_vertices + v);
				tree_node = tree_node_new(v, tree_node_depth(tree_get_node(tree, s)) + 1);
				tree_add_node(tree, tree_node);
				tree_node_add_neighbor(tree_node, tree_get_node(tree, s));
				tree_node_add_neighbor(tree_get_node(tree, s), tree_node);
				used_vertices[v] = 1;
				if (s < v)
					used_edges[edges_index_of(all_edges, all_vertices + s, all_vertices + v)] = 1;
				else
					used_edges[edges_index_of(all_edges, all_vertices + v, all_vertices + s)] = 1;
			}
		}
		array_free(neighbour);
	}
	queue_free(queue);
	free(used_vertices);
	return tree;
}


array_t *path_in_tree(tree_t *tree, vertex_t s, vertex_t g, size_t nb_vertices, size_t *all_vertices) {
	stack_t *stack_todo = stack_new();
	stack_t *stack_res = stack_new();
	size_t *used_vertices = calloc(nb_vertices, sizeof(size_t));
	array_t *path = array_new(3);
	used_vertices[s] = 1;
	stack_push(stack_todo, all_vertices + s);
	tree_node_t *tree_node;
	array_t *neighbours;
	vertex_t v;
	vertex_t res_pop = nb_vertices;
	while (!(stack_is_empty(stack_todo)) && (s != g)) {
		s = *((vertex_t *) stack_pop(stack_todo));
		if (s == res_pop) {
			stack_pop(stack_res);
		} else {
			stack_push(stack_todo, &res_pop);
			stack_push(stack_res, all_vertices + s);
			tree_node = tree_get_node(tree, s);
			neighbours = tree_node_neighbors(tree_node);
			size_t n = array_size(neighbours);
			for (size_t i = 0; i < n; i++) {
				v = *((vertex_t *) array_get(neighbours, i));
				if (used_vertices[v] == 0) {
					used_vertices[v] = 1;
					stack_push(stack_todo, all_vertices + v);
				}
			}
		}
	}
	while (!(stack_is_empty(stack_todo))) {
		stack_pop(stack_todo);
	}
	while (!(stack_is_empty(stack_res))) {
		array_add(path, stack_pop(stack_res));
	}
	stack_free(stack_todo);
	stack_free(stack_res);
	free(used_vertices);
	if (s != g) {
		array_free(path);
		return NULL;
	}
	return path;
}


array_t *
path_free(const gsl_spmatrix *csr, const struct graph_t *graph, vertex_t s, vertex_t g, size_t nb_vertices,
          size_t *all_vertices, enum color_t color) {
	stack_t *stack_todo = stack_new();
	stack_t *stack_res = stack_new();
	size_t *used_vertices = calloc(nb_vertices, sizeof(size_t));
	array_t *path = array_new(1);
	used_vertices[s] = 1;
	stack_push(stack_todo, all_vertices + s);
	array_t *neighbours;
	vertex_t v;
	vertex_t res_pop = nb_vertices;
	while (!(stack_is_empty(stack_todo)) && (s != g)) {
		s = *((vertex_t *) stack_pop(stack_todo));
		if (s == res_pop)
			stack_pop(stack_res);
		else {
			stack_push(stack_todo, &res_pop);
			stack_push(stack_res, all_vertices + s);
			neighbours = get_neighbours(csr, s, all_vertices);
			size_t n = array_size(neighbours);
			for (size_t i = 0; i < n; i++) {
				v = *((vertex_t *) array_get(neighbours, i));
				if (used_vertices[v] == 0) {
					used_vertices[v] = 1;
					if (gsl_spmatrix_get(graph->o, (color ? 0 : 1), v) == 0) {
						stack_push(stack_todo, all_vertices + v);
					}
				}
			}
			array_free(neighbours);
		}
	}
	while (!(stack_is_empty(stack_res))) {
		array_add(path, stack_pop(stack_res));
	}
	stack_free(stack_todo);
	stack_free(stack_res);
	free(used_vertices);
	if (s != g) {
		array_free(path);
		return NULL;
	}

	return path;
}

array_t *
finding_cycles(array_t *all_edges, tree_t *tree, size_t *used_edges, size_t nb_vertices, size_t *all_vertices) {
	size_t index = 0;
	array_t *path;
	edge_t *edge;
	vertex_t s;
	vertex_t g;
	cycle_t *cycle;
	array_t *cycle_basis = array_new(array_size(all_edges) - nb_vertices + 1);
	size_t n;
	for (size_t i = 0; i < array_size(all_edges); i++) {
		if (used_edges[i] == 0) {
			used_edges[i] = 1;
			edge = (edge_t *) array_get(all_edges, i);
			s = *((vertex_t *) edge_start(edge));
			g = *((vertex_t *) edge_end(edge));
			path = path_in_tree(tree, s, g, array_size(all_edges), all_vertices);
			cycle = cycle_new(index);
			array_to_cycle(cycle, path, all_vertices);
			array_add(cycle_basis, cycle);
			n = cycle_size(cycle);
			g = *cycle_get(cycle, 0);
			for (size_t j = 0; j < n; j++) {
				s = g;
				g = *cycle_get(cycle, j + 1);
				if (s < g)
					edge = (edge_t *) array_get(all_edges,
					                            edges_index_of(
							                            all_edges, all_vertices + s, all_vertices + g));
				else
					edge = (edge_t *) array_get(all_edges,
					                            edges_index_of(
							                            all_edges, all_vertices + g, all_vertices + s));
				edge_add_cycle(edge, cycle);
			}
			index++;
			array_free(path);
		}
	}
	return cycle_basis;
}

void free_cycle_basis(array_t *cycle_basis) {
	size_t size = array_size(cycle_basis);
	for (size_t i = 0; i < size; i++) {
		cycle_free(array_get(cycle_basis, i));
	}
	array_free(cycle_basis);
}


gsl_matrix *
matrix_of_cycles(array_t *all_edges, array_t *cycle_basis, const struct graph_t *graph, size_t *all_vertices,
                 enum color_t color) {
	size_t nb_cycles = array_size(cycle_basis);
	gsl_matrix *mat = gsl_matrix_calloc(nb_cycles + 1, nb_cycles + 1);

	cycle_t *main_cycle;
	array_t *second_cycle_array;
	cycle_t *second_cycle;
	size_t nb_vertices_main;
	size_t nb_cycles_second;
	vertex_t s;
	vertex_t v;
	double resist;
	double tmp_resist;
	int order;
	double tmp;
	for (size_t i = 0; i < nb_cycles; i++) {
		main_cycle = (cycle_t *) array_get(cycle_basis, i);
		nb_vertices_main = cycle_size(main_cycle);
		v = *cycle_get(main_cycle, 0);
		for (size_t j = 0; j < nb_vertices_main; j++) {
			s = v;
			v = *cycle_get(main_cycle, j + 1);
			resist = edge_resist(s, v, graph, color);
			if (s < v)
				second_cycle_array = edge_cycles(
						(edge_t *) array_get(all_edges, edges_index_of(
								all_edges, all_vertices + s, all_vertices + v)));
			else
				second_cycle_array = edge_cycles(
						(edge_t *) array_get(all_edges, edges_index_of(
								all_edges, all_vertices + v, all_vertices + s)));
			nb_cycles_second = array_size(second_cycle_array);
			for (size_t k = 0; k < nb_cycles_second; k++) {
				second_cycle = (cycle_t *) array_get(second_cycle_array, k);

				order = same_order_cycle(second_cycle, s, v);
				tmp_resist = order ? resist : -resist;
				tmp = gsl_matrix_get(mat, i, cycle_id(second_cycle));
				gsl_matrix_set(mat, i, cycle_id(second_cycle), tmp + tmp_resist);
			}
		}
	}
	return mat;
}

cycle_t *generator_cycle(const gsl_spmatrix *csr, const struct graph_t *graph, vertex_t first_component,
                         vertex_t second_component,
                         size_t nb_cycles, size_t nb_vertices, size_t *all_vertices, enum color_t color) {
	array_t *path = path_free(csr, graph, first_component, second_component, nb_vertices, all_vertices, color);
	if (path == NULL)
		return NULL;
	cycle_t *cycle = cycle_new(nb_cycles);
	array_to_cycle(cycle, path, all_vertices);
	array_free(path);
	return cycle;
}

void
matrix_with_generator(gsl_matrix *mat, array_t *all_edges, cycle_t *cycle, array_t *cycle_basis,
                      const struct graph_t *graph,
                      size_t *all_vertices, enum color_t color) {
	size_t nb_cycles = array_size(cycle_basis);
	cycle_t *second_cycle;
	array_t *second_cycle_array;
	size_t nb_vertices_main = cycle_size(cycle);
	size_t nb_cycles_second;
	vertex_t s;
	vertex_t v = *cycle_get(cycle, 0);
	double resist;
	double tmp_resist;
	int order;
	double tmp;
	for (size_t j = 0; j < nb_vertices_main - 1; j++) {
		s = v;
		v = *cycle_get(cycle, j + 1);
		resist = edge_resist(s, v, graph, color);
		if (s < v)
			second_cycle_array = edge_cycles(
					(edge_t *) array_get(all_edges, edges_index_of(
							all_edges, all_vertices + s, all_vertices + v)));
		else
			second_cycle_array = edge_cycles(
					(edge_t *) array_get(all_edges, edges_index_of(
							all_edges, all_vertices + v, all_vertices + s)));
		nb_cycles_second = array_size(second_cycle_array);
		for (size_t k = 0; k < nb_cycles_second; k++) {
			second_cycle = (cycle_t *) array_get(second_cycle_array, k);
			order = same_order_cycle(second_cycle, s, v);
			tmp_resist = order ? resist : -resist;
			tmp = gsl_matrix_get(mat, nb_cycles, cycle_id(second_cycle));
			gsl_matrix_set(mat, nb_cycles, cycle_id(second_cycle), tmp + tmp_resist);
			tmp = gsl_matrix_get(mat, cycle_id(second_cycle), nb_cycles);
			gsl_matrix_set(mat, cycle_id(second_cycle), nb_cycles, tmp + tmp_resist);
		}
		tmp = gsl_matrix_get(mat, nb_cycles, nb_cycles);
		gsl_matrix_set(mat, nb_cycles, nb_cycles, tmp + resist);
	}
}


gsl_vector *generator_vector(size_t nb_cycles) {
	gsl_vector *vec = gsl_vector_calloc(nb_cycles + 1);
	gsl_vector_set(vec, nb_cycles, 1);
	return vec;
}

double
solve_for_resistance(gsl_matrix *mat1, gsl_matrix *mat2, gsl_vector *vec1, gsl_vector *vec2, enum color_t color) {
	size_t size = mat1->size1;
	gsl_permutation *p = gsl_permutation_alloc(size);
	gsl_vector *x = gsl_vector_alloc(size);
	int signum = 0;
	gsl_linalg_LU_decomp(mat1, p, &signum);
	gsl_linalg_LU_solve(mat1, p, vec1, x);
	double res1 = fabs(gsl_vector_get(x, size - 1));
	signum = 0;
	gsl_linalg_LU_decomp(mat2, p, &signum);
	gsl_linalg_LU_solve(mat2, p, vec2, x);
	double res2 = fabs(gsl_vector_get(x, size - 1));
	gsl_permutation_free(p);
	gsl_vector_free(x);
	if (color == 0)
		return res1 / res2;
	else
		return res2 / res1;
}

double solve_for_resistance_diminished(gsl_matrix *mat, gsl_vector *vec) {
	size_t size = mat->size1;
	gsl_permutation *p = gsl_permutation_alloc(size);
	gsl_vector *x = gsl_vector_alloc(size);
	int signum = 0;
	gsl_linalg_LU_decomp(mat, p, &signum);
	gsl_linalg_LU_solve(mat, p, vec, x);
	double res = fabs(gsl_vector_get(x, size - 1));
	gsl_permutation_free(p);
	gsl_vector_free(x);
	return res;
}
