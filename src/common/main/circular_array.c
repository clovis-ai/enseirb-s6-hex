#include "circular_array.h"

circular_t *circular_new(size_t capacity) {
	return array_new(capacity);
}

void circular_free(circular_t *array) {
	array_free(array);
}

size_t circular_size(circular_t *array) {
	return array_size(array);
}

void *circular_get(circular_t *array, int index) {
	return array_get(array, index % array_size(array));
}

void circular_set(circular_t *array, int index, void *value) {
	array_set(array, index % array_size(array), value);
}

void circular_add(circular_t *array, void *value) {
	array_add(array, value);
}
