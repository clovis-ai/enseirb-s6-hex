#include <stdio.h>
#include <array.h>
#include <neighbours.h>
#include "is_winning.h"


// We use 4 global variables to stock points of each sides of the graph
// in order to find if sides are linked or not.
static int color0_left;
static int color0_right;
static int color1_left;
static int color1_right;



static void
in_depth(size_t number, gsl_spmatrix *is_visited, const struct graph_t *graph, size_t actual, gsl_spmatrix *csr,
         size_t *all_vertices) {
	array_t *neighbours = get_neighbours(csr, actual, all_vertices);
	size_t nb_neighbours = array_size(neighbours);
	vertex_t neighbour;
	for (size_t i = 0; i < nb_neighbours; i++) {
		neighbour = * (vertex_t *)  array_get(neighbours, i);
		if (gsl_spmatrix_get(graph->o, number, neighbour) && !gsl_spmatrix_get(is_visited, 0, neighbour)) {
			gsl_spmatrix_set(is_visited, 0, neighbour, 1);
			in_depth(number, is_visited, graph, neighbour, csr, all_vertices);
		}
	}
	array_free(neighbours);
}


int is_winning(const struct graph_t *graph, gsl_spmatrix *csr, size_t *all_vertices) {
	gsl_spmatrix *is_visited = gsl_spmatrix_alloc(1, graph->num_vertices);
	gsl_spmatrix_set(is_visited, 0, color0_left, 1);
	in_depth(0, is_visited, graph, color0_left, csr, all_vertices);

	if (gsl_spmatrix_get(is_visited, 0, color0_right)) {
		gsl_spmatrix_free(is_visited);
		return 1;
	}

	gsl_spmatrix_set_zero(is_visited);

	gsl_spmatrix_set(is_visited, 0, color1_left, 1);
	in_depth(1, is_visited, graph, color1_left, csr, all_vertices);

	if (gsl_spmatrix_get(is_visited, 0, color1_right)) {
		gsl_spmatrix_free(is_visited);
		return 2;
	}


	gsl_spmatrix_free(is_visited);

	gsl_spmatrix *spm = graph->o;
	vertex_t s = 0;
	while (gsl_spmatrix_get(spm, 0, s) || gsl_spmatrix_get(spm, 1, s)) {
		if (s >= graph->num_vertices - 1) {
			return 3;
		}
		s++;
	}
	return 0;
}


void is_winning_init(const struct graph_t *graph, gsl_spmatrix *csr, size_t *all_vertices) {
	gsl_spmatrix *is_visited = gsl_spmatrix_alloc(1, graph->num_vertices);
	color0_left = -1;
	color1_left = -1;
	for (size_t i = 0; i < graph->num_vertices; i++) {
		if (gsl_spmatrix_get(graph->o, 0, i) && !gsl_spmatrix_get(is_visited, 0, i)) {
			if (color0_left == -1) {
				color0_left = i;
				gsl_spmatrix_set(is_visited, 0, i, 1);
				in_depth(0, is_visited, graph, i, csr, all_vertices);
			} else
				color0_right = i;
		}

		if (gsl_spmatrix_get(graph->o, 1, i) && !gsl_spmatrix_get(is_visited, 0, i)) {
			if (color1_left == -1) {
				color1_left = i;
				gsl_spmatrix_set(is_visited, 0, i, 1);
				in_depth(1, is_visited, graph, i, csr, all_vertices);
			} else
				color1_right = i;
		}
	}
	gsl_spmatrix_free(is_visited);
}
