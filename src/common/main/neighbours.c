#include "neighbours.h"

size_t number_neighbours(const gsl_spmatrix* spm, vertex_t i) {
	return spm->p[i + 1] - spm->p[i];
}

array_t *get_neighbours(const gsl_spmatrix* csr, vertex_t s, vertex_t *V) {
	size_t n = number_neighbours(csr, s);
	array_t *neighbour = array_new(n);
	for (vertex_t j = 0; j < n; j++) {
		array_add(neighbour, V + csr->i[csr->p[s] + j]);
	}
	return neighbour;
}
