#include "array.h"
#include <malloc.h>

struct array {
	size_t size;
	size_t capacity;
	void **elements;
};

struct array *array_new(size_t capacity) {
	if (capacity <= 0)
		return 0;

	struct array *array = malloc(sizeof(*array));

	array->size = 0;
	array->capacity = capacity;
	array->elements = malloc(sizeof(array->elements) * capacity);

	for (size_t i = 0; i < capacity; ++i) {
		array->elements[i] = 0;
	}

	return array;
}

void array_free(struct array *array) {
	free(array->elements);
	free(array);
}

size_t array_size(struct array *array) {
	return array->size;
}

void *array_get(struct array *array, size_t index) {
	if (index >= array->size)
		return 0;

	return array->elements[index];
}

void array_set(struct array *array, size_t index, void *value) {
	if (index >= array->size)
		return;

	array->elements[index] = value;
}

size_t array_capacity(array_t *array) {
	return array->capacity;
}

void array_add(array_t *array, void *value) {
	if (array->size >= array->capacity) {
		array->elements = realloc(array->elements, sizeof(array->elements) * (array->capacity *= 2lu));
	}

	array->elements[array->size++] = value;
}
