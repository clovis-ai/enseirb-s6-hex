#include <array.h>
#include "stack.h"
#include <malloc.h>

struct stack {
	array_t *array;
	size_t cursor;
};

stack_t *stack_new(void) {
	stack_t *stack = malloc(sizeof(*stack));

	stack->array = array_new(1);
	stack->cursor = 0;

	return stack;
}

void stack_free(stack_t *stack) {
	array_free(stack->array);
	free(stack);
}

void stack_push(stack_t *stack, void *value) {
	size_t size = array_size(stack->array);

	if (stack->cursor < size) {
		array_set(stack->array, stack->cursor, value);
	} else {
		array_add(stack->array, value);
	}
	stack->cursor++;
}

void *stack_pop(stack_t *stack) {
	if (!stack_is_empty(stack)) {
		stack->cursor--;
		void *value = array_get(stack->array, stack->cursor);
		array_set(stack->array, stack->cursor, 0);
		return value;
	} else return 0;
}

int stack_is_empty(stack_t *stack) {
	return stack->cursor <= 0;
}
