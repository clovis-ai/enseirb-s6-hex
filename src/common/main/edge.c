#include <malloc.h>
#include "edge.h"

struct edge {
	vertex_t *start;
	vertex_t *end;
	array_t *cycles;
};

edge_t *edge_new(vertex_t *start, vertex_t *end) {
	edge_t *edge = malloc(sizeof(*edge));

	edge->start = start;
	edge->end = end;
	edge->cycles = array_new(1);
	return edge;
}

void edge_free(edge_t *edge) {
	array_free(edge->cycles);
	free(edge);
}

vertex_t *edge_start(edge_t *edge) {
	return edge->start;
}

vertex_t *edge_end(edge_t *edge) {
	return edge->end;
}

int edges_index_of(array_t *array, const vertex_t *first, const vertex_t *second) {
	size_t size = array_size(array);
	size_t min = 0;
	size_t max = size - 1;

	while (min <= max && max <= size) {
		size_t i = min + (max - min) / 2;
		edge_t *current = array_get(array, i);

		if (*current->start == *first && *current->end == *second)
			return i;
		else if ((*current->start < *first)  || ((*current->start == *first) && (*current->end < *second)))
			min = i + 1;
		else
			max = i - 1;
	}

	return -1;
}

array_t *edge_cycles(edge_t *edge) {
	return edge->cycles;
}

void edge_add_cycle(edge_t *edge, cycle_t *cycle) {
	array_add(edge->cycles, cycle);
}
