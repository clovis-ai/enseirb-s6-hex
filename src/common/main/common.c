#include <stdio.h>
#include <graph.h>
#include <move.h>
#include "common.h"
#include <gsl/gsl_spmatrix.h>

void common_say_hello() {
	printf("Common module loaded.\n");
}

int is_valid(struct move_t *move, enum color_t color, struct graph_t *graph) {

	if (move->c != color)
		return 0;

	if (move->m >= graph->num_vertices)
		return 0;

	if (gsl_spmatrix_get(graph->o, color, move->m) != 0)
		return 0;

	return 1;
}


void apply_move(struct move_t *move, struct graph_t *graph) {
	gsl_spmatrix_set(graph->o, move->c, move->m, 1);
}

