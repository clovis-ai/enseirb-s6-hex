#include <stack.h>
#include <malloc.h>
#include "queue.h"

struct queue {
	stack_t *in;
	stack_t *out;
};

queue_t *queue_new(void) {
	queue_t *queue = malloc(sizeof(*queue));

	queue->in = stack_new();
	queue->out = stack_new();

	return queue;
}

void queue_free(queue_t *queue) {
	stack_free(queue->in);
	stack_free(queue->out);

	free(queue);
}

void queue_push(queue_t *queue, void *value) {
	stack_push(queue->out, value);
}

void *queue_pop(queue_t *queue) {
	if (!queue_is_empty(queue)) {
		if (stack_is_empty(queue->in)) {
			void *value;
			while ((value = stack_pop(queue->out)))
				stack_push(queue->in, value);
		}
		return stack_pop(queue->in);
	} else return 0;
}

int queue_is_empty(queue_t *queue) {
	return stack_is_empty(queue->in) && stack_is_empty(queue->out);
}
