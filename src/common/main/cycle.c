#include "cycle.h"
#include <malloc.h>

struct cycle {
	size_t id;
	circular_t *vertices;
};

cycle_t *cycle_new(size_t id) {
	cycle_t *ret = malloc(sizeof(*ret));

	ret->id = id;
	ret->vertices = circular_new(3);

	return ret;
}

void cycle_free(cycle_t *cycle) {
	circular_free(cycle->vertices);
	free(cycle);
}

size_t cycle_id(cycle_t *cycle) {
	return cycle->id;
}

size_t cycle_size(cycle_t *cycle) {
	return circular_size(cycle->vertices);
}

vertex_t *cycle_get(cycle_t *cycle, size_t index) {
	return circular_get(cycle->vertices, index);
}

void cycle_set(cycle_t *cycle, size_t index, vertex_t *vertex) {
	circular_set(cycle->vertices, index, vertex);
}

void cycle_add(cycle_t *cycle, vertex_t *vertex) {
	circular_add(cycle->vertices, vertex);
}
