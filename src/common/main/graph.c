#include <graph.h>

void free_graph(struct graph_t *g) {
	gsl_spmatrix_free(g->t);
	gsl_spmatrix_free(g->o);
	free(g);
}


struct graph_t *copy_graph(const struct graph_t *graph) {
	struct graph_t *ret = malloc(sizeof(*graph));
	ret->num_vertices = graph->num_vertices;
	ret->o = gsl_spmatrix_alloc(2, ret->num_vertices);
	ret->t = gsl_spmatrix_alloc(ret->num_vertices, ret->num_vertices);
	gsl_spmatrix_memcpy(ret->o, graph->o);
	gsl_spmatrix_memcpy(ret->t, graph->t);
	return ret;
}
