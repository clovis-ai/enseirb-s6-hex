#include <stdio.h>
#include "server_test.h"
#include "random_test.h"
#include "kirchoff_test.h"
#include "line_test.h"
#include "common_test.h"
#include "manual_test.h"

int manual_tests();

int main() {
	printf("Running tests...\n");

	int nbr_failures = 0;

	nbr_failures += common_tests();
	nbr_failures += random_tests();
	nbr_failures += manual_tests();
	nbr_failures += kirchoff_tests();
	nbr_failures += line_tests();
	nbr_failures += server_tests();


	printf("\nAll tests ran, %s%d failures%s.\n",
	       nbr_failures == 0 ? "\x1b[32m" : "\x1b[31m",
	       nbr_failures,
	       "\x1b[0m");
	return nbr_failures;
}
