#include <stdio.h>
#include <test_suite.h>
#include "is_winning.h"
#include <graph.h>
#include <move.h>
#include <common.h>
#include <gsl/gsl_spmatrix.h>
#include <array.h>
#include <circular_array.h>
#include <cycle.h>
#include <stack.h>
#include <queue.h>
#include <edge.h>
#include <tree.h>
#include <neighbours.h>
#include <kirchhoff_functions.h>
#include <time.h>
#include "../../server/main/graph_init.h"

static int test_is_valid_color(void) {
	test_start("is valid color");

	struct move_t move = {1, 0};
	struct graph_t graph;

	graph.num_vertices = 2;
	graph.o = gsl_spmatrix_alloc(2, 2);

	if (is_valid(&move, 0, &graph) == 0 || is_valid(&move, 1, &graph) == 1) {
		gsl_spmatrix_free(graph.o);
		return test_failure("Problem in evaluation of color");
	}

	gsl_spmatrix_free(graph.o);
	return test_success();
}

static int test_is_valid_place(void) {
	test_start("is valid place");

	struct move_t move = {1, 0};
	struct graph_t graph;

	graph.num_vertices = 10;
	graph.o = gsl_spmatrix_alloc(2, 10);

	gsl_spmatrix_set(graph.o, 0, 1, 1);

	if (is_valid(&move, 0, &graph)) {
		gsl_spmatrix_free(graph.o);
		return test_failure("Accept an invalid position move");
	}

	gsl_spmatrix_free(graph.o);
	return test_success();
}

static int test_is_valid_inside_graph(void) {
	test_start("is valid inside graph");

	struct graph_t graph;

	size_t k = 10;

	graph.num_vertices = k;
	graph.o = gsl_spmatrix_alloc(2, k);

	struct move_t move1 = {k + 1, 0};
	struct move_t move2 = {6 * k, 0};

	if (is_valid(&move1, 0, &graph) || is_valid(&move2, 0, &graph)) {
		gsl_spmatrix_free(graph.o);
		return test_failure("Accept a move outside the graph");
	}

	gsl_spmatrix_free(graph.o);
	return test_success();
}

static int test_apply_move_basic(void) {
	test_start("apply move basic");

	size_t k = 10;

	struct graph_t graph;
	graph.num_vertices = k;
	graph.o = gsl_spmatrix_alloc(2, k);

	struct move_t move = {5, 0};

	apply_move(&move, &graph);

	if (gsl_spmatrix_get(graph.o, 0, 5) != 1) {
		gsl_spmatrix_free(graph.o);
		return test_failure("No apply");
	}
	gsl_spmatrix_free(graph.o);
	return test_success();
}

static int test_apply_move_random(void) {
	test_start("apply move random");

	size_t k = 10;

	struct graph_t graph;
	graph.num_vertices = k;
	graph.o = gsl_spmatrix_alloc(2, k);

	size_t m = rand() % 42;
	int c = rand() % 2;
	struct move_t move = {m, c};

	apply_move(&move, &graph);

	if (gsl_spmatrix_get(graph.o, c, m) != 1) {
		gsl_spmatrix_free(graph.o);
		return test_failure("Not the right apply");
	}

	gsl_spmatrix_free(graph.o);
	return test_success();
}

int empty_hex_graph_winning_test() {
	test_start("empty_graph_hex_winning");

	struct graph_t *g = generate_empty_graph(5, 'h');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);

	is_winning_init(g, csr, all_vertices);

	if (is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Empty graph is winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int empty_square_graph_winning_test() {
	test_start("empty_graph_square_winning");

	struct graph_t *g = generate_empty_graph(5, 'c');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	is_winning_init(g, csr, all_vertices);

	if (is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Empty graph is winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int empty_triangle_graph_winning_test() {
	test_start("empty_graph_triangle_winning");

	struct graph_t *g = generate_empty_graph(6, 't');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	is_winning_init(g, csr, all_vertices);

	if (is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Empty graph is winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int full_hex_graph_winning_test() {
	test_start("full_graph_hex_winning");

	struct graph_t *g = generate_empty_graph(5, 'h');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	is_winning_init(g, csr, all_vertices);

	for (size_t i = 0; i < g->num_vertices; i++) {
		gsl_spmatrix_set(g->o, 0, i, 1);
		gsl_spmatrix_set(g->o, 1, i, 1);
	}
	if (!is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Full graph is not winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int full_square_graph_winning_test() {
	test_start("full_graph_square_winning");

	struct graph_t *g = generate_empty_graph(5, 'c');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	is_winning_init(g, csr, all_vertices);

	for (size_t i = 0; i < g->num_vertices; i++) {
		gsl_spmatrix_set(g->o, 0, i, 1);
		gsl_spmatrix_set(g->o, 1, i, 1);
	}
	if (!is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Full graph is not winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int full_triangle_graph_winning_test() {
	test_start("full_graph_triangle_winning");

	struct graph_t *g = generate_empty_graph(4, 't');
	gsl_spmatrix *spm = g->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	is_winning_init(g, csr, all_vertices);

	for (size_t i = 0; i < g->num_vertices; i++) {
		gsl_spmatrix_set(g->o, 0, i, 1);
		gsl_spmatrix_set(g->o, 1, i, 1);
	}
	if (!is_winning(g, csr, all_vertices)) {
		free_graph(g);
		free(all_vertices);
		gsl_spmatrix_free(csr);
		return test_failure("Full graph is not winning");
	}
	free(all_vertices);
	gsl_spmatrix_free(csr);
	free_graph(g);
	return test_success();
}

int array_creation() {
	int nbr_failures = 0;

	int a = 5;
	int b = 6;
	int c = 10;
	int d = 3;

	test_start("array_new");
	array_t *array = array_new(4);
	nbr_failures += array ? test_success() : test_failure("Could not allocate the array");

	test_start("array_get & array_add");
	array_add(array, &a);
	array_add(array, &b);
	array_add(array, &c);
	array_add(array, &d);

	nbr_failures += &a == array_get(array, 0)
	                && &b == array_get(array, 1)
	                && &c == array_get(array, 2)
	                && &d == array_get(array, 3)
	                ? test_success() : test_failure("Wrong result on get operation.");

	test_start("array_set");
	array_set(array, 0, &d);
	nbr_failures += &d == array_get(array, 0)
	                ? test_success() : test_failure("Wrong result on get operation.");

	test_start("array expansion");
	int e = 2;
	int f = 3;
	array_add(array, &e);
	array_add(array, &f);
	nbr_failures += &e == array_get(array, 4)
	                && &f == array_get(array, 5)
	                ? test_success() : test_failure("Wrong result on get operation.");

	array_free(array);
	return nbr_failures;
}

int circular_array() {
	int nbr_failures = 0;

	int a = 15;
	int b = 11;

	test_start("circular_get in");
	circular_t *array = circular_new(2);
	circular_add(array, &a);
	circular_add(array, &b);
	nbr_failures += &a == circular_get(array, 0)
	                && &b == circular_get(array, 1)
	                ? test_success() : test_failure("Wrong result on get operation.");

	test_start("circular_get out");
	nbr_failures += &a == circular_get(array, 2)
	                && &b == circular_get(array, 3)
	                && &b == circular_get(array, -1)
	                ? test_success() : test_failure("Wrong result on get operation.");

	circular_free(array);
	return nbr_failures;
}

int cycle_test() {
	int nbr_failures = 0;

	test_start("cycle_id");
	cycle_t *cycle = cycle_new(1);
	nbr_failures += cycle_id(cycle) == 1 ? test_success() : test_failure("The ID should've been 1.");

	test_start("cycle_add & cycle_get");
	vertex_t a = 0;
	vertex_t b = 1;
	vertex_t c = 2;
	vertex_t d = 3;
	cycle_add(cycle, &a);
	cycle_add(cycle, &b);
	cycle_add(cycle, &c);
	cycle_add(cycle, &d);
	nbr_failures += &a == cycle_get(cycle, 0)
	                && &b == cycle_get(cycle, 1)
	                && &c == cycle_get(cycle, 2)
	                && &d == cycle_get(cycle, 3)
	                ? test_success() : test_failure("Wrong result on get operation");

	cycle_free(cycle);
	return nbr_failures;
}

int stack_test() {
	int nbr_failures = 0;

	test_start("stack_is_empty");
	stack_t *stack = stack_new();
	nbr_failures += stack_is_empty(stack) ? test_success() : test_failure("Stack should be empty when created.");

	test_start("stack_push & stack_pop");
	int a = 2;
	int b = 3;
	int c = 15;
	stack_push(stack, &a);
	stack_push(stack, &b);
	stack_push(stack, &c);
	nbr_failures += &c == stack_pop(stack)
	                && &b == stack_pop(stack)
	                && &a == stack_pop(stack)
	                && stack_is_empty(stack)
	                && 0 == stack_pop(stack)
	                ? test_success() : test_failure("Could not get and pop correctly from the stack.");

	test_start("stack_push & stack_pop 2");
	stack_push(stack, &c);
	nbr_failures += &c == stack_pop(stack)
	                && stack_is_empty(stack)
	                && 0 == stack_pop(stack)
	                ? test_success() : test_failure("Could not get and pop correct from the stack.");

	stack_free(stack);
	return nbr_failures;
}

int queue_test() {
	int nbr_failures = 0;

	test_start("queue");
	queue_t *queue = queue_new();
	int a = 5;
	int b = 14;
	int c = 13;
	queue_push(queue, &a);
	queue_push(queue, &b);
	queue_push(queue, &c);
	nbr_failures += &a == queue_pop(queue)
	                && &b == queue_pop(queue)
	                && &c == queue_pop(queue)
	                && queue_is_empty(queue)
	                && 0 == queue_pop(queue)
	                ? test_success() : test_failure("Could not push and pop correctly from the queue.");

	queue_free(queue);
	return nbr_failures;
}

int edge_test() {
	int nbr_failures = 0;

	test_start("edge");
	size_t v1 = 2;
	size_t v2 = 3;
	edge_t *edge = edge_new(&v1, &v2);
	nbr_failures += v1 == *edge_start(edge)
	                && v2 == *edge_end(edge)
	                ? test_success() : test_failure("Could not get start & end of the edge.");

	edge_free(edge);
	return nbr_failures;
}

int tree_test() {
	int nbr_failures = 0;

	test_start("tree_add & tree_get");
	tree_t *tree = tree_new(4);
	tree_node_t *n1 = tree_node_new(0, 5);
	tree_node_t *n2 = tree_node_new(1, 5);
	tree_node_t *n3 = tree_node_new(2, 5);
	tree_node_t *n4 = tree_node_new(3, 5);
	tree_add_node(tree, n1);
	tree_add_node(tree, n2);
	tree_add_node(tree, n3);
	tree_add_node(tree, n4);
	nbr_failures += n1 == tree_get_node(tree, tree_node_id(n1))
	                && n2 == tree_get_node(tree, tree_node_id(n2))
	                && n3 == tree_get_node(tree, tree_node_id(n3))
	                && n4 == tree_get_node(tree, tree_node_id(n4))
	                ? test_success() : test_failure("Could not get nodes from the tree.");

	test_start("tree_neighbors");
	tree_node_add_neighbor(n1, n2);
	tree_node_add_neighbor(n2, n1);
	array_t *neighbors = tree_node_neighbors(n1);
	nbr_failures += array_size(neighbors) == 1
	                && n2 == array_get(neighbors, 0)
	                ? test_success() : test_failure("Could not find the neighbor.");

	tree_free(tree);
	return nbr_failures;
}

int test_edges_index_of() {
	int nbr_failures = 0;

	vertex_t v[] = {0, 1, 2, 3};
	vertex_t *v1 = v, *v2 = v + 1, *v3 = v + 2, *v4 = v + 3;

	edge_t *e12 = edge_new(v1, v2);
	edge_t *e34 = edge_new(v3, v4);

	array_t *array = array_new(1);
	array_add(array, e12);
	array_add(array, e34);

	test_start("edges_index_of in 1");
	nbr_failures += edges_index_of(array, v1, v2) == 0
	                ? test_success() : test_failure("The edge 1-2 should be found");

	test_start("edges_index_of in 2");
	nbr_failures += edges_index_of(array, v3, v4) == 1
	                ? test_success() : test_failure("The edge 3-4 should be found");

	test_start("edges_index_of in");
	nbr_failures += edges_index_of(array, v1, v4) == -1
	                ? test_success() : test_failure("The edge 1-2 should be found");

	array_free(array);
	edge_free(e12);
	edge_free(e34);
	return nbr_failures;
}

int get_neighbours_test() {
	test_start("get_neighbours");
	size_t n = 4;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	gsl_spmatrix *spm = generate_graph_adjacency(n, 't');
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *array;
	for (size_t i = 0; i < nb_vertices; i++) {
		array = get_neighbours(csr, i, V);
		for (size_t j = 0; j < array_size(array); j++) {
			if (!gsl_spmatrix_get(spm, i, *((size_t *) array_get(array, j)))) {
				test_failure("Neighbour get found too many neighbours");
				return 1;
			}
			gsl_spmatrix_set(spm, i, *((size_t *) array_get(array, j)), 0);
		}
		for (size_t j = 0; j < nb_vertices; j++) {
			if (gsl_spmatrix_get(spm, i, j)) {
					test_failure("Neighbour get didn't find all neighbours");
					return 1;
			}
		}
		array_free(array);
	}
	gsl_spmatrix_free(spm);
	gsl_spmatrix_free(csr);
	free(V);
	test_success();
	return 0;
}

int component_finding_test() {
	test_start("component_finding");
	size_t n = 4;
	size_t nb_vertices = n * n;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	gsl_spmatrix *spm = generate_graph_adjacency(n, 'h');
	gsl_spmatrix *ownership = generate_graph_ownership(n, 'h');
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	gsl_spmatrix *is_visited = gsl_spmatrix_alloc(1, nb_vertices);
	component_finding(ownership, csr, is_visited, 0, 0, V);
	for (size_t i = 0; i < n; i++) {
		for (size_t j = 0; j < n; j++) {
			if (i == 0 && j < n - 1) {
				if (!gsl_spmatrix_get(is_visited, 0, i * n + j)) {
					test_failure("Didn't find all vertices of the first component");
					printf("failed to find %ld %ld \n", i, j);
					return 1;
				}
			} else if (gsl_spmatrix_get(is_visited, 0, i * n + j)) {
				test_failure("Found too many vertices of the first component");
				printf("too many is %ld %ld \n", i, j);
				return 1;
			}
		}
	}
	gsl_spmatrix_set_zero(is_visited);
	component_finding(ownership, csr, is_visited, n - 1, 1, V);
	for (size_t i = 0; i < n; i++) {
		for (size_t j = 0; j < n; j++) {
			if (i != n - 1 && j == n - 1) {
				if (!gsl_spmatrix_get(is_visited, 0, i * n + j)) {
					test_failure("Didn't find all vertex of the second component");
					printf("failed to find %ld %ld \n", i, j);
					return 1;
				}
			} else if (gsl_spmatrix_get(is_visited, 0, i * n + j)) {
				test_failure("Found too many vertices of the second component");
				printf("too many is %ld %ld \n", i, j);
				return 1;
			}
		}
	}
	gsl_spmatrix_free(spm);
	gsl_spmatrix_free(csr);
	gsl_spmatrix_free(is_visited);
	gsl_spmatrix_free(ownership);
	free(V);
	test_success();
	return 0;
}

int init_components_test() {
	test_start("component init");
	size_t m = 4;
	struct graph_t *graph = generate_empty_graph(m, 'h');
	size_t nb_vertices = graph->num_vertices;
	vertex_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	vertex_t s1, s2, v1, v2;
	init_components(graph, csr, &s1, &v1, &s2, &v2, V);
	if (s1 != 0) {
		test_failure("Failed to find the first component of the first player");
		printf("%ld \n", s1);
		return 1;
	}
	if (v1 != (m - 1) * m + 1) {
		test_failure("Failed to find the second component of the first player");
		printf("%ld \n", v1);
		return 1;
	}
	if (s2 != m - 1) {
		test_failure("Failed to find the first component of the second player");
		printf("%ld \n", s2);
		return 1;
	}
	if (v2 != m) {
		test_failure("Failed to find the second component of the second player");
		printf("%ld \n", v2);
		return 1;
	}
	test_success();
	free_graph(graph);
	free(V);
	gsl_spmatrix_free(csr);
	return 0;
}

int list_edges_test() {
	test_start("list edges");
	size_t n = 4;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	gsl_spmatrix *spm = generate_graph_adjacency(n, 't');
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *edges = list_edges(csr, V);
	for (size_t i = 0; i <  nb_vertices; i++) {
		for (size_t j = i; j <  nb_vertices; j++) {
			if (edges_index_of(edges, V + i, V + j) != -1) {
				if (! gsl_spmatrix_get(spm, i, j)) {
					test_failure("Gets an edge that doesn't exist");
					printf("%ld %ld\n", i, j);
					return 1;
				}
				gsl_spmatrix_set(spm, i, j, 0);
			}
		}
	}
	for (size_t i = 0; i <  nb_vertices; i++) {
		for (size_t j = i; j <  nb_vertices; j++) {
			if (gsl_spmatrix_get(spm, i, j)) {
				test_failure("Misses an edge");
				printf("%ld %ld\n", i, j);
				return 1;
			}
		}
	}
	gsl_spmatrix_free(spm);
	gsl_spmatrix_free(csr);
	free(V);
	free_list_edges(edges);
	test_success();
	return 0;
}


int edge_resist_test() {
	test_start("edge resist");
	size_t n = 10;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 't');
	gsl_spmatrix *t = graph->t;
	gsl_spmatrix *o = graph->o;
	double thresist1;
	double acresist1;
	double thresist2;
	double acresist2;
	for (vertex_t i = 0; i <  nb_vertices; i++) {
		for (vertex_t j = 0; j < nb_vertices; j++) {
			if (gsl_spmatrix_get(t, i, j))
			{
				thresist1 = edge_resist(i, j, graph, 0);
				thresist2 = edge_resist(i, j, graph, 1);
				acresist1 = 0;
				acresist2 = 0;
				if (gsl_spmatrix_get(o, 0, i)) {
					acresist1 += 0.01;
					acresist2 += 100;
				}
				else if (gsl_spmatrix_get(o, 1, i)) {
					acresist2 += 0.01;
					acresist1 += 100;
				}
				else {
					acresist2 += 1;
					acresist1 += 1;
				}
				if (gsl_spmatrix_get(o, 0, j)) {
					acresist1 += 0.01;
					acresist2 += 100;
				}
				else if (gsl_spmatrix_get(o, 1, j)) {
					acresist2 += 0.01;
					acresist1 += 100;
				}
				else {
					acresist2 += 1;
					acresist1 += 1;
				}
				if (acresist1 != thresist1 || acresist2 != thresist2) {
					test_failure("Edge resist was wrong");
					printf("%ld %ld\n", i, j);
					return 1;
				}

			}
		}
	}
	test_success();
	free(V);
	free_graph(graph);
	return 0;
}



int array_to_cycle_test() {
	test_start("array to cycle");
	size_t nb_vertices = 50;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	array_t *array = array_new(nb_vertices);
	for (vertex_t i = 0; i < nb_vertices; i++) {
		array_add(array, V + nb_vertices - i - 1);
	}
	cycle_t *cycle = cycle_new(nb_vertices);
	array_to_cycle(cycle, array, V);
	for (vertex_t i = 0; i < nb_vertices; i++) {
		if (*cycle_get(cycle, i) != nb_vertices - 1 - i) {
			test_failure("Array_to_cycle failed");
			printf("%ld\n", i);
			return 1;
		}
	}
	test_success();
	free(V);
	array_free(array);
	cycle_free(cycle);
	return 0;
}



int same_order_cycle_test() {
	test_start("same order cycle");
	size_t nb_vertices = 50;
	size_t m = 5;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	cycle_t *cycle = cycle_new(5);
	for (vertex_t i = 0; i < m; i++) {
		cycle_add(cycle, V + i);
	}
	for (vertex_t i = 0; i < m - 1; i++) {
		if (same_order_cycle(cycle, i, i + 1) != 1) {
			test_failure("Same order failed");
			printf("%ld %ld\n", i, i + 1);
			return 1;
		}
		if (same_order_cycle(cycle, i + 1, i) != 0) {
			test_failure("Opposite order failed");
			printf("%ld %ld\n", i, i + 1);
			return 1;
		}
	}
	if (same_order_cycle(cycle, 1, 3) != -1) {
		test_failure("Not in cycle failed");
		printf("%d %d\n", 1, 3);
		return 1;
	}
	test_success();
	free(V);
	cycle_free(cycle);
	return 0;
}



int path_in_tree_test() {
	test_start("path in tree");
	size_t nb_vertices = 10;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	tree_t *tree = tree_new(nb_vertices);
	tree_node_t *tree_node = tree_node_new(0, 0);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(1, 1);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(2, 1);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(3, 2);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(4, 2);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(5, 2);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(6, 2);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(7, 3);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(8, 3);
	tree_add_node(tree, tree_node);
	tree_node = tree_node_new(9, 0);
	tree_add_node(tree, tree_node);
	tree_node_add_neighbor(tree_get_node(tree, 7), tree_get_node(tree, 4));
	tree_node_add_neighbor(tree_get_node(tree, 4), tree_get_node(tree, 7));
	tree_node_add_neighbor(tree_get_node(tree, 4), tree_get_node(tree, 1));
	tree_node_add_neighbor(tree_get_node(tree, 1), tree_get_node(tree, 4));
	tree_node_add_neighbor(tree_get_node(tree, 8), tree_get_node(tree, 5));
	tree_node_add_neighbor(tree_get_node(tree, 5), tree_get_node(tree, 8));
	tree_node_add_neighbor(tree_get_node(tree, 1), tree_get_node(tree, 5));
	tree_node_add_neighbor(tree_get_node(tree, 5), tree_get_node(tree, 1));
	tree_node_add_neighbor(tree_get_node(tree, 0), tree_get_node(tree, 1));
	tree_node_add_neighbor(tree_get_node(tree, 1), tree_get_node(tree, 0));
	tree_node_add_neighbor(tree_get_node(tree, 0), tree_get_node(tree, 2));
	tree_node_add_neighbor(tree_get_node(tree, 2), tree_get_node(tree, 0));
	tree_node_add_neighbor(tree_get_node(tree, 2), tree_get_node(tree, 3));
	tree_node_add_neighbor(tree_get_node(tree, 3), tree_get_node(tree, 2));
	tree_node_add_neighbor(tree_get_node(tree, 2), tree_get_node(tree, 6));
	tree_node_add_neighbor(tree_get_node(tree, 6), tree_get_node(tree, 2));
	array_t *path = path_in_tree(tree, 3, 7, nb_vertices, V);
	if (*(vertex_t *)array_get(path, 0) != 7) {
		test_failure("Path is incorrect");
		printf("0 is %ld should be 7\n", *(vertex_t *)array_get(path, 0));
		return 1;
	}
	if (*(vertex_t *)array_get(path, 1) != 4) {
		test_failure("Path is incorrect");
		printf("1 is %ld should be 4\n", *(vertex_t *)array_get(path, 1));
		return 1;
	}
	if (*(vertex_t *)array_get(path, 2) != 1) {
		test_failure("Path is incorrect");
		printf("2 is %ld should be 1\n", *(vertex_t *)array_get(path, 2));
		return 1;
	}
	if (*(vertex_t *)array_get(path, 3) != 0) {
		test_failure("Path is incorrect");
		printf("3 is %ld should be 0\n", *(vertex_t *)array_get(path, 3));
		return 1;
	}
	if (*(vertex_t *)array_get(path, 4) != 2) {
		test_failure("Path is incorrect");
		printf("4 is %ld should be 2\n", *(vertex_t *)array_get(path, 4));
		return 1;
	}if (*(vertex_t *)array_get(path, 5) != 3) {
		test_failure("Path is incorrect");
		printf("5 is %ld should be 3\n", *(vertex_t *)array_get(path, 5));
		return 1;
	}
	array_free(path);
	if (path_in_tree(tree, 0, 9, nb_vertices, V) != NULL) {
		test_failure("Can't handle no path");
		return 1;
	}
	test_success();
	free(V);
	tree_free(tree);
	return 0;
}

int spanning_tree_test() {
	test_start("spanning tree");
	size_t n = 6;
	size_t nb_vertices = n * n;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 'c');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr;
	csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *neighbours;
	size_t nb_neighbour;
	vertex_t s;
	int index;
	for (size_t i = 0; i < nb_vertices; i++) {
		if (i != tree_node_id(tree_get_node(tree, i))) {
			test_failure("Tree_node isn't well placed");
			printf("%ld\n", i);
			return 1;
		}
		for (size_t j = 0; j < nb_vertices; j++) {
			if (i != j) {
				array_t *path = path_in_tree(tree, i, j, nb_vertices, V);
				if (path == NULL) {
					test_failure("Tree is not connected");
					printf("%ld %ld\n", i, j);
					return 1;
				}
				else
					array_free(path);
			}
		}
		neighbours = tree_node_neighbors(tree_get_node(tree, i));
		nb_neighbour = array_size(neighbours);
		for (size_t j = 0; j < nb_neighbour; j++) {
			s = *((vertex_t *) array_get(neighbours, j));
			if (! gsl_spmatrix_get(spm, i, s)) {
				test_failure("False edge is used");
				printf("%ld %ld\n", i, s);
				return 1;
			}
			if (i < s) {
				index = edges_index_of(E, V + i, V + s);
				if (!used_edges[index]) {
					test_failure("Used edge is not in used_edge");
					printf("%ld %ld\n", i, s);
					return 1;
				}
				used_edges[index] = 0;
			}
			array_set(neighbours, j, V + i);
			if (path_in_tree(tree, i, s, nb_vertices, V) != NULL) {
				array_t *path = path_in_tree(tree, i, s, nb_vertices, V);
				size_t size = array_size(path);
				printf("size is %ld\n", size);
				for (size_t k = 0; k < size; k++)
					printf("Path is %ld\n", *(vertex_t *) array_get(path, k));
				test_failure("Tree is cyclical");
				printf("%ld %ld\n", i, s);
				return 1;
			}
			array_set(neighbours, j, V + s);
		}
	}
	for (size_t i = 0; i < array_size(E); i++) {
		if (used_edges[i]) {
			test_failure("Unused edge is in used_edge");
			printf("%ld\n", i);
			return 1;
		}
	}
	test_success();
	free_list_edges(E);
	free(V);
	free(used_edges);
	tree_free(tree);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	return 0;
}

int finding_cycles_test() {
	test_start("finding cycles");
	size_t n = 10;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	gsl_spmatrix *spm = generate_graph_adjacency(n, 't');
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	size_t *used_edges2 = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *cycle_basis = finding_cycles(E, tree, used_edges, nb_vertices, V);
	size_t size = array_size(E) - nb_vertices + 1;
	cycle_t *cycle;
	size_t counter;
	size_t nb_edges;
	vertex_t v1;
	vertex_t v2;
	int index;
	if (array_size(cycle_basis) != size) {
		test_failure("Cycle basis size is incorrect");
		return 1;
	}
	for (size_t i = 0; i < size; i++) {
		cycle = (cycle_t *) array_get(cycle_basis, i);
		if (cycle_id(cycle) != i) {
			test_failure("Cycle isn't well placed");
			printf("%ld\n", i);
			return 1;
		}
		counter = 0;
		nb_edges = cycle_size(cycle);
		v2 = *cycle_get(cycle, 0);
		for (size_t j = 0; j < nb_edges; j ++) {
			v1 = v2;
			v2 = *cycle_get(cycle, j + 1);
			if (v1 < v2)
				index = edges_index_of(E, V + v1, V + v2);
			else
				index = edges_index_of(E, V + v2, V + v1);
			if (used_edges2[index] == 0) {
				used_edges2[index] = 1;
				counter = 1;
			}
		}
		if (counter == 0) {
			test_failure("Cycle is not linearly independent from others");
			printf("%ld\n", i);
			return 1;
		}
	}
	for (size_t i = 0; i < array_size(E); i++) {
		if (used_edges2[i] == 0) {
			test_failure("An edge is not being used");
			printf("%ld\n", i);
			return 1;
		}
	}
	tree_free(tree);
	free(used_edges);
	free(used_edges2);
	free(V);
	free_list_edges(E);
	gsl_spmatrix_free(spm);
	gsl_spmatrix_free(csr);
	free_cycle_basis(cycle_basis);
	test_success();
	return 0;
}


int path_free_test() {
	test_start("path free");
	size_t m = 10;
	size_t nb_vertices = 100;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(m, 'c');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *E = list_edges(csr, V);
	array_t *path = path_free(csr, graph, 2, 7, nb_vertices, V, 0);
	vertex_t v1;
	vertex_t v2 = (*(vertex_t *) array_get(path, 0));
	int index;
	for (size_t i = 0; i < array_size(path) - 1; i++) {
		v1 = v2;
		v2 = (*(vertex_t *) array_get(path, i + 1));
		if (gsl_spmatrix_get(graph->o, 1, v1)) {
			test_failure("Vertex is used twice or is owned by the opposite player");
			printf("%ld\n", v1);
			return 1;
		}
		if (v1 < v2)
			index = edges_index_of(E, V + v1, V + v2);
		else
			index = edges_index_of(E, V + v2, V + v1);
		if (index == -1) {
			test_failure("An edge used does not exist");
			printf("%ld %ld\n", v1, v2);
			return 1;
		}
	}
	array_free(path);
	path = path_free(csr, graph, 2, 7, nb_vertices, V, 1);
	if (path != NULL) {
		array_free(path);
		test_failure("Can't handle no path");
		return 1;
	}
	test_success();
	free(V);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_list_edges(E);
	return 0;
}

int path_free_test2() {
	test_start("path free 2");
	size_t m = 3;
	size_t nb_vertices = 9;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(m, 'c');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *E = list_edges(csr, V);
	array_t *path = path_free(csr, graph, 0, 7, nb_vertices, V, 0);
	vertex_t v1;
	vertex_t v2 = (*(vertex_t *) array_get(path, 0));
	int index;
	for (size_t i = 0; i < array_size(path) - 1; i++) {
		v1 = v2;
		v2 = (*(vertex_t *) array_get(path, i + 1));
		if (gsl_spmatrix_get(graph->o, 1, v1)) {
			test_failure("Vertex is used twice or is owned by the opposite player");
			printf("%ld\n", v1);
			return 1;
		}
		if (v1 < v2)
			index = edges_index_of(E, V + v1, V + v2);
		else
			index = edges_index_of(E, V + v2, V + v1);
		if (index == -1) {
			test_failure("An edge used does not exist");
			printf("%ld %ld\n", v1, v2);
			return 1;
		}
	}
	array_free(path);
	if (path_free(csr, graph, 2, 7, nb_vertices, V, 1) != NULL) {
		test_failure("Can't handle no path");
		return 1;
	}
	test_success();
	free(V);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_list_edges(E);
	return 0;
}

int matrix_of_cycles_test() {
	test_start("matrix cycles");
	size_t n = 2;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 't');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *cycle_basis = finding_cycles(E, tree, used_edges, nb_vertices, V);
	size_t size = array_size(E) - nb_vertices + 1 + 1;
	gsl_matrix *mat = matrix_of_cycles(E, cycle_basis, graph, V, 0);
	if (mat->size1 != size) {
		test_failure("Matrix cycle size is incorrect");
		return 1;
	}
/*	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			printf("%lf  ", gsl_matrix_get(mat, i, j));
		}
		printf("\n");
	}*/
	double tmp1;
	double tmp2;
	double diff;
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			tmp1 = gsl_matrix_get(mat, i, j);
			tmp2 = gsl_matrix_get(mat, j, i);
			diff = tmp1 - tmp2;
			diff = (diff > 0) ? diff : -diff;
			if (diff >= 0.01) {
				test_failure("Matrix cycle is not symmetric");
				printf("Differences are %ld %ld %lf %lf\n", i, j, tmp1, tmp2);
				return 1;
			}
		}
	}
	tree_free(tree);
	free(used_edges);
	free(V);
	free_list_edges(E);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_cycle_basis(cycle_basis);
	gsl_matrix_free(mat);
	test_success();
	return 0;
}

int matrix_with_generator_test() {
	test_start("matrix generator");
	size_t n = 6;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 't');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	vertex_t s1, v1, s2, v2;
	init_components(graph, csr, &s1, &v1, &s2, &v2, V);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *cycle_basis = finding_cycles(E, tree, used_edges, nb_vertices, V);
	size_t size = array_size(E) - nb_vertices + 1 + 1;
	gsl_matrix *mat1 = matrix_of_cycles(E, cycle_basis, graph, V, 0);
	gsl_matrix *mat2 = matrix_of_cycles(E, cycle_basis, graph, V, 1);
	cycle_t *cycle1 = generator_cycle(csr, graph, s1, v1, size - 1, nb_vertices, V, 0);
	cycle_t *cycle2 = generator_cycle(csr, graph, s2, v2, size - 1, nb_vertices, V, 1);
	matrix_with_generator(mat1, E, cycle1, cycle_basis, graph, V, 0);
	matrix_with_generator(mat2, E, cycle2, cycle_basis, graph, V, 1);
	if (mat1->size1 != size) {
		test_failure("Matrix generator size is incorrect");
		return 1;
	}
	double tmp1;
	double tmp2;
	double diff;
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			tmp1 = gsl_matrix_get(mat2, i, j);
			tmp2 = gsl_matrix_get(mat2, j, i);
			diff = tmp1 - tmp2;
			diff = (diff > 0) ? diff : -diff;
			if (diff >= 0.01) {
				test_failure("Matrix cycle is not symmetric");
				printf("Differences are %ld %ld %lf %lf\n", i, j, tmp1, tmp2);
				return 1;
			}
		}
	}
	tree_free(tree);
	free(used_edges);
	free(V);
	free_list_edges(E);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_cycle_basis(cycle_basis);
	cycle_free(cycle1);
	cycle_free(cycle2);
	gsl_matrix_free(mat1);
	gsl_matrix_free(mat2);
	test_success();
	return 0;
}


int solve_for_resistance_test() {
	test_start("solve resistance");
	size_t n = 4;
	size_t nb_vertices = n * n * 6;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 't');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	vertex_t s1, v1, s2, v2;
	init_components(graph, csr, &s1, &v1, &s2, &v2, V);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *cycle_basis = finding_cycles(E, tree, used_edges, nb_vertices, V);
	size_t size = array_size(E) - nb_vertices + 1 + 1;


	gsl_matrix *mat1 = matrix_of_cycles(E, cycle_basis, graph, V, 0);
	gsl_matrix *mat2 = matrix_of_cycles(E, cycle_basis, graph, V, 1);
	cycle_t *cycle1 = generator_cycle(csr, graph, s1, v1, size - 1, nb_vertices, V, 0);
	cycle_t *cycle2 = generator_cycle(csr, graph, s2, v2, size - 1, nb_vertices, V, 1);
	matrix_with_generator(mat1, E, cycle1, cycle_basis, graph, V, 0);
	matrix_with_generator(mat2, E, cycle2, cycle_basis, graph, V, 1);
	gsl_vector *vec1 = generator_vector(size - 1);
	gsl_vector *vec2 = generator_vector(size - 1);
	gsl_matrix *mat1cp = gsl_matrix_alloc(size, size);
	gsl_matrix *mat2cp = gsl_matrix_alloc(size, size);
	gsl_matrix_memcpy(mat1cp, mat1);
	gsl_matrix_memcpy(mat2cp, mat2);
	gsl_vector *vec1cp = generator_vector(size - 1);
	gsl_vector *vec2cp = generator_vector(size - 1);
	double resistance1 = solve_for_resistance(mat1, mat2, vec1, vec2, 0);
	double resistance2 = solve_for_resistance(mat1cp, mat2cp, vec1cp, vec2cp, 1);
	double_t diff = resistance1 - 1 / resistance2;
	diff = (diff > 0) ? diff : -diff;
	if (diff >= 0.01) {
		test_failure("Relation between resistances is not verified");
		return 1;
	}
	diff = resistance1 - 1.;
	diff = (diff > 0) ? diff : -diff;
	if (diff >= 0.1) {
		test_failure("Symmetric board doesn't have a Kirchhoff value of 1");
		return 1;
	}
	gsl_spmatrix_set(graph->o, 1, nb_vertices / 2, 1);
	gsl_matrix *mat3 = matrix_of_cycles(E, cycle_basis, graph, V, 0);
	cycle_t *cycle3 = generator_cycle(csr, graph, s1, v1, size - 1, nb_vertices, V, 0);
	matrix_with_generator(mat3, E, cycle3, cycle_basis, graph, V, 0);
	gsl_vector *vec3 = generator_vector(size - 1);
	gsl_matrix *mat4 = matrix_of_cycles(E, cycle_basis, graph, V, 1);
	cycle_t *cycle4 = generator_cycle(csr, graph, s2, v2, size - 1, nb_vertices, V, 1);
	matrix_with_generator(mat4, E, cycle4, cycle_basis, graph, V, 1);
	gsl_vector *vec4 = generator_vector(size - 1);
	double resistance3 = solve_for_resistance(mat3, mat4, vec3, vec4, 1);
	if (resistance3 < resistance2) {
		test_failure("Kirchhoff value doesn't increase as intended");
		return 1;
	}
	tree_free(tree);
	free(used_edges);

	free(V);
	free_list_edges(E);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_cycle_basis(cycle_basis);
	cycle_free(cycle1);
	cycle_free(cycle2);
	gsl_matrix_free(mat1);
	gsl_matrix_free(mat2);
	gsl_matrix_free(mat1cp);
	gsl_matrix_free(mat2cp);
	gsl_vector_free(vec1);
	gsl_vector_free(vec2);
	gsl_vector_free(vec1cp);
	gsl_vector_free(vec2cp);
	cycle_free(cycle3);
	cycle_free(cycle4);
	gsl_matrix_free(mat3);
	gsl_matrix_free(mat4);
	gsl_vector_free(vec3);
	gsl_vector_free(vec4);

	test_success();
	return 0;
}

int solve_for_resistance_time() {
	clock_t start, end, actualend;
	start = clock();
	test_start("solve resistance");
	size_t n = 6;
	size_t nb_vertices = n * n;
	size_t *V = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		V[i] = i;
	}
	struct graph_t *graph = generate_empty_graph(n, 'h');
	gsl_spmatrix *spm = graph->t;
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);
	vertex_t s1, v1, s2, v2;
	init_components(graph, csr, &s1, &v1, &s2, &v2, V);
	array_t *E = list_edges(csr, V);
	size_t *used_edges = calloc(array_size(E), sizeof(size_t));
	tree_t *tree = spanning_tree(csr, 0, used_edges, V, E);
	array_t *cycle_basis = finding_cycles(E, tree, used_edges, nb_vertices, V);
	size_t size = array_size(E) - nb_vertices + 1 + 1;
	end = clock();
	printf("%Lf\n", (long double)(end - start) / CLOCKS_PER_SEC);
	double max_Kirchhoff = 0;
	double tmp_Kirchhoff;
	size_t max_vertex;
	for (size_t i = 0; i < nb_vertices; i++) {
		if (! ((gsl_spmatrix_get(graph->o, 0, i) == 1 || gsl_spmatrix_get(graph->o, 1, i) == 1))) {
			gsl_spmatrix_set(graph->o, 0, i, 1);
			gsl_matrix *mat1 = matrix_of_cycles(E, cycle_basis, graph, V, 0);
			gsl_matrix *mat2 = matrix_of_cycles(E, cycle_basis, graph, V, 1);
			cycle_t *cycle1 = generator_cycle(csr, graph, s1, v1, size - 1, nb_vertices, V, 0);
			cycle_t *cycle2 = generator_cycle(csr, graph, s2, v2, size - 1, nb_vertices, V, 1);
			matrix_with_generator(mat1, E, cycle1, cycle_basis, graph, V, 0);
			matrix_with_generator(mat2, E, cycle2, cycle_basis, graph, V, 1);
			gsl_vector *vec1 = generator_vector(size - 1);
			gsl_vector *vec2 = generator_vector(size - 1);
			tmp_Kirchhoff = solve_for_resistance(mat1, mat2, vec1, vec2, 0);
			if (tmp_Kirchhoff > max_Kirchhoff)
			{
				max_Kirchhoff = tmp_Kirchhoff;
				max_vertex = i;
			}
			gsl_spmatrix_set(graph->o, 0, i, 0);
			cycle_free(cycle1);
			cycle_free(cycle2);
			gsl_matrix_free(mat1);
			gsl_matrix_free(mat2);
			gsl_vector_free(vec1);
			gsl_vector_free(vec2);
		}
	}
	actualend = clock();
	printf("%Lf\n", (long double)(actualend - end) / CLOCKS_PER_SEC);

	printf("%ld\n", max_vertex);
	tree_free(tree);
	free(used_edges);

	free(V);
	free_list_edges(E);
	free_graph(graph);
	gsl_spmatrix_free(csr);
	free_cycle_basis(cycle_basis);

	test_success();
	return 0;

}

int common_tests() {
	test_suite_start("common");

	int nbr_failures = 0;

	nbr_failures += test_is_valid_color();
	nbr_failures += test_is_valid_place();
	nbr_failures += test_is_valid_inside_graph();
	nbr_failures += test_apply_move_basic();
	nbr_failures += test_apply_move_random();
	nbr_failures += empty_hex_graph_winning_test();
	nbr_failures += empty_square_graph_winning_test();
	nbr_failures += empty_triangle_graph_winning_test();
	nbr_failures += full_hex_graph_winning_test();
	nbr_failures += full_square_graph_winning_test();
	nbr_failures += full_triangle_graph_winning_test();

	test_suite_start("data structures");
	nbr_failures += array_creation();
	nbr_failures += circular_array();
	nbr_failures += cycle_test();
	nbr_failures += stack_test();
	nbr_failures += queue_test();
	nbr_failures += edge_test();
	nbr_failures += tree_test();
	nbr_failures += test_edges_index_of();

	test_suite_start("kirchhoff functions");
	nbr_failures += get_neighbours_test();
	nbr_failures += component_finding_test();
	nbr_failures += init_components_test();
	nbr_failures += list_edges_test();
	nbr_failures += edge_resist_test();
	nbr_failures += array_to_cycle_test();
	nbr_failures += same_order_cycle_test();
	nbr_failures += path_in_tree_test();
	nbr_failures += spanning_tree_test();
	nbr_failures += path_free_test();
	nbr_failures += path_free_test2();
	nbr_failures += finding_cycles_test();
	nbr_failures += matrix_of_cycles_test();
	nbr_failures += matrix_with_generator_test();
	nbr_failures += solve_for_resistance_test();
	//nbr_failures += solve_for_resistance_time();

	return nbr_failures;
}

