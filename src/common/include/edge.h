#ifndef HEX_ENSEIRB_S6_EDGE_H
#define HEX_ENSEIRB_S6_EDGE_H

/** @file */

#include "cycle.h"

/**
 * Represents an edge on a graph; with a starting vertex and an ending vertex.
 */
typedef struct edge edge_t;

/**
 * Creates a new edge.
 * @param start Where it starts from.
 * @param end Where it ends at.
 * @return A new edge, that needs to be freed later.
 */
edge_t *edge_new(vertex_t *start, vertex_t *end);

/**
 * Frees an edge.
 * @param edge The edge to be freed.
 */
void edge_free(edge_t *edge);

/**
 * Gets the start of an edge.
 * @param edge The edge you want the start of.
 * @return The start of an edge.
 */
vertex_t *edge_start(edge_t *edge);

/**
 * Gets the end of an edge.
 * @param edge The edge you want the end of.
 * @return The end of an edge.
 */
vertex_t *edge_end(edge_t *edge);

/**
 * Searches for an edge formed by two vertices.
 *
 * This function assumes that the array is sorted in increasing value of edges.
 *
 * @param array An array of edge_t values, that should be searched.
 * @param first A vertex
 * @param second Another vertex
 * @return Either the index of the edge formed by the two vertices, or -1 if there's no such edge.
 */
int edges_index_of(array_t *array, const vertex_t *first, const vertex_t *second);

/**
 * Gets an array of all cycles that were added to the edge
 * If used as in Kirchhoff functions, gets all the cycles that possess this edge
 * @param edge The edge you want the cycles of
 * @return The array of cycles
 */
array_t *edge_cycles(edge_t *edge);

/**
 * Add a cycle to the list of cycles that are returned by edge_cycle
 * The caller is responsible to free the element.
 *
 * @param edge The edge you'd like to add a cycle to.
 * @param cycle The cycle to be added to the edge.
 */
void edge_add_cycle(edge_t *edge, cycle_t *cycle);

#endif //HEX_ENSEIRB_S6_EDGE_H
