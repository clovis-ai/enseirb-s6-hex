#ifndef ACTOR_ENSEIRB_S6_COMMON_H
#define ACTOR_ENSEIRB_S6_COMMON_H

/** @file */

#include <graph.h>
#include <move.h>

void common_say_hello();

/**
 * Verify a move if a move is valid
 * @param move the move to try
 * @param color the color of the player doing the move
 * @param graph the graph of the graph in which the move is try to be played
 * @return A boolean 1 if the move is valid, else 0
 */
int is_valid(struct move_t *move, enum color_t color, struct graph_t *graph);

/**
 * Apply a move
 * @param m the move applied
 * @param g the graph in which the move is played
 */
void apply_move(struct move_t *move, struct graph_t *graph);

#endif //ACTOR_ENSEIRB_S6_COMMON_H
