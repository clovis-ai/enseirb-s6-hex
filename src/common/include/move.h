#ifndef HEX_ENSEIRB_S6_MOVE_H
#define HEX_ENSEIRB_S6_MOVE_H

/** @file */

#include <stdlib.h>

/**
 * Players
 */
enum color_t { BLACK = 0, WHITE = 1, NO_COLOR = 2 };

/**
 * A representation of a move including the color of the player who's played it.
 */
struct move_t {

	/**
	 * The position of the move this structure represents
	 */
	size_t m;

	/**
	 * The color who has played this move
	*/
	enum color_t c;
};

#endif //HEX_ENSEIRB_S6_MOVE_H
