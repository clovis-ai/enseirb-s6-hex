#ifndef HEX_ENSEIRB_S6_STACK_H
#define HEX_ENSEIRB_S6_STACK_H

/** @file */

/**
 * A stack implementation.
 */
typedef struct stack stack_t;

/**
 * Creates a new empty stack.
 * @return A new empty stack, that you should free.
 */
stack_t *stack_new(void);

/**
 * Frees a stack.
 * @param stack The stack to be freed.
 */
void stack_free(stack_t *stack);

/**
 * Add an element to a stack.
 * @param stack The stack you'd like to add an element to.
 * @param value The element you want to add.
 */
void stack_push(stack_t *stack, void *value);

/**
 * Remove the first element of a stack.
 * @param stack The stack you'd like to remove an element from.
 * @return The first element of the stack, or a null pointer if there isn't any.
 */
void *stack_pop(stack_t *stack);

/**
 * Is this stack empty?
 * @param stack The stack you'd like information from.
 * @return A boolean as to whether the stack is empty or not.
 */
int stack_is_empty(stack_t *stack);

#endif //HEX_ENSEIRB_S6_STACK_H
