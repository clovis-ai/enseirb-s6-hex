#ifndef HEX_ENSEIRB_S6_NEIGHBOURS_H
#define HEX_ENSEIRB_S6_NEIGHBOURS_H

/** @file */

#include "array.h"
#include "cycle.h"
#include <gsl/gsl_spmatrix.h>

/**
 * Returns an array containing the neighbours of s given the adjacency matrix csr
 * The neighbours are out of order !
 * @param csr The adjacency matrix
 * @param s The vertex of whom you would like to know the neighbours
 * @param V An array of size_t to allow the representation of size_t inside
 * the array which can only contain pointers
 * @return An array of vertex_t * neighbours
 */
array_t *get_neighbours(const gsl_spmatrix* csr, vertex_t s, vertex_t *V);

#endif //HEX_ENSEIRB_S6_NEIGHBOURS_H

