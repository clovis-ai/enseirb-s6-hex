#ifndef HEX_ENSEIRB_S6_CYCLE_H
#define HEX_ENSEIRB_S6_CYCLE_H

/** @file */

#include "circular_array.h"

typedef size_t vertex_t;

/**
 * A cycle in a graph.
 *
 * See circular.h for the implementation details.
 */
typedef struct cycle cycle_t;

/**
 * A new cycle, in a graph.
 * @param id The ID of a cycle.
 * @return A new cycle of default capacity 3.
 */
cycle_t *cycle_new(size_t id);

/**
 * Frees a cycle.
 * @param cycle The cycle to be freed.
 */
void cycle_free(cycle_t *cycle);

/**
 * Gets the ID of a cycle.
 * @param cycle The cycle you want the size of.
 * @return The cycle's ID.
 */
size_t cycle_id(cycle_t *cycle);

/**
 * Gets the size of a cycle.
 * @param cycle The cycle you want the size of.
 * @return The cycle's size.
 */
size_t cycle_size(cycle_t *cycle);

/**
 * Gets a vertex from a cycle.
 * @param cycle The cycle you want a vertex from.
 * @param index The vertex' index. Can be any valid number, see circular.h
 * @return A vertex from the cycle.
 */
vertex_t *cycle_get(cycle_t *cycle, size_t index);

/**
 * Sets a vertex in a cycle.
 * @param cycle The cycle you want to set a vertex in to.
 * @param index The index you want to put the vertex at.
 * @param vertex The vertex you want to add.
 */
void cycle_set(cycle_t *cycle, size_t index, vertex_t *vertex);

/**
 * Adds a vertex to a cycle, with a resize if necessary.
 * @param cycle The cycle you want to add a vertex to.
 * @param vertex The vertex you want to add.
 */
void cycle_add(cycle_t *cycle, vertex_t *vertex);

#endif //HEX_ENSEIRB_S6_CYCLE_H
