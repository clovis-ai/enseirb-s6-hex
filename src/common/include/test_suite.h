#ifndef ACTOR_ENSEIRB_S6_TEST_SUITE_H
#define ACTOR_ENSEIRB_S6_TEST_SUITE_H

/** @file */

/**
 * Announces the creation of a test suite in the standard output.
 * @param name the name of the test suite
 */
void test_suite_start(char *name);

/**
 * Announces the start of a test in the standard output.
 * @param name the name of the test
 */
void test_start(char *name);

/**
 * Announces the success of the last created test.
 * @return the number of failures, which is always 0
 */
int test_success();

/**
 * Announces the failure of the last created test.
 * @param message the reason why the test failed
 * @return the number of failures, which is always 1
 */
int test_failure(char *message);

#endif //ACTOR_ENSEIRB_S6_TEST_SUITE_H
