#ifndef HEX_ENSEIRB_S6_GRAPH_H
#define HEX_ENSEIRB_S6_GRAPH_H

/** @file */

#include <stdlib.h>
#include <gsl/gsl_spmatrix.h>

/**
 * The board of the game.
 */
struct graph_t {

	/**
	 * Number of vertices in the graph (letter n in Graph Theory)
	 */
	size_t num_vertices;

	/**
	 * Representation of the game board.
	 *
	 * This is a sparse matrix of size num_vertices², in which:
	 * @code{.c}
	 * t[i][j] == 1
	 * @endcode means that there is an edge i → j.
	 */
	gsl_spmatrix* t;

	/**
	 * Representation of the ownership of each player.
	 *
	 * This is a sparse matrix of size 2*num_vertices, in which:
	 * @code{.c}
	 * o[p][i] == 1
	 * @endcode means that the vertex i is owned by p.
	 */
	gsl_spmatrix* o;
};


/**
 * Free the @param graph graph and its spmatrix
 */
void free_graph(struct graph_t *graph);



/**
 * Copies a graph. The caller has the responsibility to free the returned object.
 * @param graph The graph to be copied.
 * @return The copy.
 */
struct graph_t *copy_graph(const struct graph_t *graph);

#endif //HEX_ENSEIRB_S6_GRAPH_H
