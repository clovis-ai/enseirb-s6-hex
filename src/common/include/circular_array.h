#ifndef HEX_ENSEIRB_S6_CIRCULAR_ARRAY_H
#define HEX_ENSEIRB_S6_CIRCULAR_ARRAY_H

/** @file */

#include <stdlib.h>
#include "array.h"

/**
 * A generic handle to a circular array, indexed from 0 to its size (exclusive).
 *
 * The array is circular in that requests for elements outside of its size will
 * loop.
 */
typedef array_t circular_t;

/**
 * Creates a new circular array.
 *
 * @param capacity The original capacity of the circular array. It should not be 0.
 * @return A new heap-allocated array.
 */
circular_t *circular_new(size_t capacity);

/**
 * Frees a circular array.
 *
 * @param array The array to be freed.
 */
void circular_free(circular_t *array);

/**
 * Returns the effective size of a circular array.
 *
 * @param array The array you'd like the size of.
 * @return The size of the array.
 */
size_t circular_size(circular_t *array);

/**
 * Gets the element at an index of the array.
 *
 * @param array The array you'd like to get an element from.
 * @param index The index of the element you want.
 * @return The element at the designated position, or a null pointer if there isn't any.
 */
void *circular_get(circular_t *array, int index);

/**
 * Sets the element at an index of the array.
 *
 * If there is already an element at that position, the previous element is lost.
 *
 * The caller is responsible from the correct freeing of the inserted element.
 *
 * @param array The array you'd like to set an element of.
 * @param index The index of the element you want to input.
 * @param value The element which should be placed in the array.
 */
void circular_set(circular_t *array, int index, void *value);

/**
 * Add an element to the array. The caller is responsible to free the element.
 *
 * The array will be resized if needed.
 *
 * @param array The array you'd like to add an element to.
 * @param value The value to be added to the array.
 */
void circular_add(circular_t *array, void *value);

#endif //HEX_ENSEIRB_S6_CIRCULAR_ARRAY_H
