#ifndef HEX_ENSEIRB_S6_KIRCHHOFF_FUNCTIONS_H
#define HEX_ENSEIRB_S6_KIRCHHOFF_FUNCTIONS_H

/** @file */

#include <stdlib.h>
#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <gsl/gsl_spmatrix.h>
#include <tree.h>
#include <queue.h>
#include <neighbours.h>
#include <stack.h>
#include <edge.h>
#include <gsl/gsl_linalg.h>

/**
 * Fills the matrix is_visited with the vertices that are part of the component of s
 * The component includes only vertices that belong to the player side
 * @param ownership The ownership matrix
 * @param csr The adjacency matrix, in CSR format
 * @param is_visited The boolean matrix to be filled (should be full of zeros at first)
 * @param s The vertex whose component we are interested in
 * @param side The number of the player we are dealing with
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * a stack which can only contain pointers
 */
void component_finding(gsl_spmatrix *ownership, gsl_spmatrix *csr, gsl_spmatrix *is_visited, vertex_t s, size_t side,
                       vertex_t *all_vertices);

/**
 * Set the vertex_t *s1, *v1, *s2 and *v2 to be part of the four original components
 * Vertices with 1 are owned by the first player and same for the second player.
 * This function needs to be used at initialisation.
 * @param graph The graph of the play board
 * @param csr The adjacency matrix, in CSR format
 * @param s1 A pointer to a vertex_t that will be set to be a vertex owned by the first player in the first component
 * @param v1 A pointer to a vertex_t that will be set to be a vertex owned by the first player in the second component
 * @param s2 A pointer to a vertex_t that will be set to be a vertex owned by the second player in the first component
 * @param v2 A pointer to a vertex_t that will be set to be a vertex owned by the second player in the second component
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * a stack which can only contain pointers
 */
void
init_components(const struct graph_t *graph, gsl_spmatrix *csr, vertex_t *s1, vertex_t *v1, vertex_t *s2, vertex_t *v2,
                vertex_t *all_vertices);

/**
 * Gets the resistance of an edge according to a mesh representation
 * @param s The first vertex of the edge
 * @param v The second vertex of the edge
 * @param graph The graph of the game
 * @param color The color of the player whose point of vue we see through
 * @return The resistance of the edge
 */
double edge_resist(vertex_t s, vertex_t v, const struct graph_t *graph, enum color_t color);

/**
 * Fills a cycle with all the vertices of an array
 * @param dest The cycle that will be filled
 * @param src The array containing the vertices
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 */
void array_to_cycle(cycle_t *dest, array_t *src, vertex_t *all_vertices);

/**
 * Finds if the cycle possess the edge (s, v) or the edge (v, s)
 * @param cycle The cycle we are looking in
 * @param s The beginning of the edge
 * @param v The end of the edge
 * @return 1 if the cycle possess the edge (s, v)
 * @return 0 if the cycle possess the edge (v, s)
 * @return -1 if the cycle possess neither
 */
int same_order_cycle(cycle_t *cycle, vertex_t s, vertex_t v);

/**
 * Generates an array of edges containing all the edges represented in the adjacency matrix csr
 * The edges are positioned in the array in increasing order first with the start of the edge than with the end of it
 * The array is not freed !
 * @param csr The adjacency matrix, in CSR format
 * @param all_vertices A table of size_t to allow the representation of vertex_t * inside
 * the edges which can only contain pointers
 */
array_t *list_edges(const gsl_spmatrix *csr, vertex_t *all_vertices);

/**
 * Frees an array generated or whose structure is similar to the one returned by list_edges
 * Does not free the cycles, which are meant to be freed as part of cycle_basis
 * @param all_edges The array to be freed.
 */
void free_list_edges(array_t *all_edges);

/**
 * Generates a spanning tree of the graph represented by the adjacency matrix csr
 * The tree serves as an array to the tree_nodes which are connected to each other
 * used_edges is filled with 1 at the index if the edge == all_edges[i] is used to make the tree
 * @param csr The adjacency matrix, in CSR format
 * @param s The vertex that will serve as the root of the tree, it has no special value later on
 * @param used_edges A table of booleans precallocated
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * a queue which can only contain pointers
 * @param all_edges An array of all the edges in the board, used_edges[i]==1 => the ith edge of all_edges is used
 * @return the spanning tree generated
 */
tree_t *spanning_tree(const gsl_spmatrix *csr, size_t s, size_t *used_edges, size_t *all_vertices, array_t *all_edges);

/**
 * Generates an array of vertices that represent a path between the vertex g and the vertex using
 * only edges that belong to the tree.
 * The search is made from v to g but the result is in the opposite order.
 * @param tree The tree that is being searched
 * @param s The end vertex of the array
 * @param g The beginning vertex of the array
 * @param nb_vertices The number of vertices in the tree
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @return An array containing the path from g to s
 */
array_t *path_in_tree(tree_t *tree, vertex_t s, vertex_t g, size_t nb_vertices, size_t *all_vertices);

/**
 * Generates an array of vertices that represent a path between the vertex g and the vertex s using
 * only edges that do not belong to the opponent of color.
 * The search is made from v to g but the result is in the opposite order.
 * The user is responsible for giving correct vertices, that are ones that are not owned by the opposite player.
 * @param csr The adjacency matrix of the board, in CSR format
 * @param graph The graph containing the ownership of the vertices.
 * @param s The end vertex of the array
 * @param g The beginning vertex of the array
 * @param nb_vertices The number of vertices in the tree
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @param color The color of the player we are interested in
 * @return An array containing the path from g to s. The pointer NULL if no path is possible from g to s.
 */
array_t *
path_free(const gsl_spmatrix *csr, const struct graph_t *graph, vertex_t s, vertex_t g, size_t nb_vertices,
          size_t *all_vertices,
          enum color_t color);

/**
 * Generates a cycle basis of the graph using a spanning tree, and generating a cycle for each unused edge
 * of the graph to make the tree. The tree and the used_edges table should be generated by spanning_tree
 * @param all_edges An array of all the edges
 * @param tree A spanning tree of the graph
 * @param used_edges A table of booleans representing whether or not a certain edge has been used (1 is used)
 * @param nb_vertices The number of vertices in the tree
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @return An array containing a cycle basis of the graph of which tree is a spanning tree and all_edges is the list of edges
 */
array_t *finding_cycles(array_t *all_edges, tree_t *tree, size_t *used_edges, size_t nb_vertices, size_t *all_vertices);

/**
 * Frees an array of cycles as well as every cycles that are part of it
 * @param cycle_basis The array of cycles to be freed
 */
void free_cycle_basis(array_t *cycle_basis);

/**
 * Generates a matrix which represent the left terms of the linear system of Kirchhoff of a graph by point of vue of
 * the player color. Given the cycle basis of a graph and a list of the edges of the graph.
 * The matrix is of size number of cycles + 1 and is only filled in the space of number of cycles.
 * The remaining space is meant to add the generator cycle.
 * @param all_edges An array of all the edges, each edge will be filled with its cycles
 * @param cycle_basis A cycle basis of the graph
 * @param graph The graph containing the ownership of the vertices.
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @param color The color (or identifier) of the player we are interested in
 * @return The almost filled matrix of Kirchhoff, representing all the cycles of the cycle basis.
 */
gsl_matrix *
matrix_of_cycles(array_t *all_edges, array_t *cycle_basis, const struct graph_t *graph, size_t *all_vertices,
                 enum color_t color);

/**
 * Generates a cycle of vertices that contains a path between the vertex first_component and the vertex second_component
 * using only edges that do not belong to the opponent of color.
 * The search is made from v to g but the result is in the opposite order, and the result is given in a cycle.
 * The user is responsible for giving correct vertices, that are ones that are not owned by the opposite player.
 * @param csr The adjacency matrix of the board, in CSR format
 * @param graph The graph containing the ownership of the vertices.
 * @param first_component A vertex belonging to the first component of the player color.
 * @param second_component A vertex belonging to the second component of the player color.
 * @param nb_cycles The dimension of the cycle_basis of the graph
 * @param nb_vertices The number of vertices in the graph
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @param color The color (or identifier) of the player we are interested in
 * @return A cycle containing the path from g to s. The pointer NULL if no path is possible from g to s.
 */
cycle_t *generator_cycle(const gsl_spmatrix *csr, const struct graph_t *graph, vertex_t first_component,
                         vertex_t second_component,
                         size_t nb_cycles, size_t nb_vertices, size_t *all_vertices, enum color_t color);

/**
 * Adds the matrix representation of the generator_cycle in the matrix_of_cycles.
 * @param mat The matrix_of_cycles
 * @param all_edges An array of all the edges, each edge will be filled with its cycles
 * @param cycle The generator_cycle
 * @param cycle_basis The cycle basis of the graph
 * @param graph The graph containing the ownership of the vertices.
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 * @param color The color (or identifier) of the player we are interested in
 */
void
matrix_with_generator(gsl_matrix *mat, array_t *all_edges, cycle_t *cycle, array_t *cycle_basis,
                      const struct graph_t *graph,
                      size_t *all_vertices, enum color_t color);

/**
 * Generates a vector containing only zero except on the last position where a 0 is.
 * Its size is nb_cycles + 1. It stand for the tension vector if used as intended by the function kirchhoff_resistance
 * @param nb_cycles The number of cycles in the graph
 * @return The vector of tension of the graph
 */
gsl_vector *generator_vector(size_t nb_cycles);

/**
 * Given two linear systems, solve them, extract the last coordinate of both solution vector, and divide them.
 * Of both coordinate, which is the numerator and which is the denominator is based on the value of color.
 * This way, we obtain the value of the board from the point of vue of the player color.
 * @param mat1 The matrix of all cycles (cycle_basis and generator_cycle) of the first player
 * @param mat2 The matrix of all cycles (cycle_basis and generator_cycle) of the second player
 * @param vec1 A vector of tension obtained through generator_vector
 * @param vec2 A vector of tension obtained through generator_vector
 * @param color The color (or identifier) of the player we are interested in
 * @return The value of the board from the point of vue of the player color.
 */
double solve_for_resistance(gsl_matrix *mat1, gsl_matrix *mat2, gsl_vector *vec1, gsl_vector *vec2, enum color_t color);

/**
 * Gives the inverse of the resistance of only one side of the board.
 * To be used to finish the game once the resistance of the opponent is infinite.
 * @param mat e matrix of all cycles (cycle_basis and generator_cycle) of the player
 * @param vec A vector of tension obtained through generator_vector
 * @return The value of the board from the point of vue of the player.
 */
double solve_for_resistance_diminished(gsl_matrix *mat, gsl_vector *vec);

#endif //HEX_ENSEIRB_S6_KIRCHHOFF_FUNCTIONS_H
