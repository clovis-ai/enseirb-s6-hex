#ifndef IS_WINNING_H
#define IS_WINNING_H

/** @file */

#include <stdlib.h>
#include <gsl/gsl_spmatrix.h>
#include "graph.h"

/**
 * Initialize values to find members of sides of the graph.
 * Have to use it at the beginning of the game to be allowed to use is_winnning.
 * @param graph take the initial graph.
 * @param csr A matrix of adjacency in the csr format so as to access neighbours in constant time
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 */
void is_winning_init(const struct graph_t *graph, gsl_spmatrix *csr, size_t *all_vertices);

/**
 * Return 1 if the graph is in a winning configuration for color 0
 * Return 2 if the graph is in a winning configuration for color 1
 * Return 0 if the graph isn't in a winning configuration
 * Return 3 if the graph is full without a winner (there is a draw)
 * @param graph take the current graph of the game.
 * @param csr A matrix of adjacency in the csr format so as to access neighbours in constant time
 * @param all_vertices A table of size_t to allow the representation of size_t inside
 * an array which can only contain pointers
 */
int is_winning(const struct graph_t *graph, gsl_spmatrix *csr, size_t *all_vertices);


#endif //IS_WINNIG_H
