#ifndef HEX_ENSEIRB_S6_ARRAY_H
#define HEX_ENSEIRB_S6_ARRAY_H

/** @file */

#include <stdlib.h>

/**
 * A generic handle to an array object, indexed from 0 to its size (exclusive).
 *
 * The 'array_add' function can be used to add new elements; the array will be resized accordingly.
 */
typedef struct array array_t;

/**
 * Creates a new array.
 *
 * @param capacity The original capacity of the array. It should not be 0.
 * @return A new heap-allocated array.
 */
array_t *array_new(size_t capacity);

/**
 * Frees an array.
 *
 * @param array The array to be freed.
 */
void array_free(array_t *array);

/**
 * Returns the size of an array.
 *
 * @param array The array you'd like the size of.
 * @return The size of the array.
 */
size_t array_size(array_t *array);

/**
 * Returns the capacity of an array.
 *
 * @param array The array you'd like the capacity of.
 * @return The capacity of the array.
 */
size_t array_capacity(array_t *array);

/**
 * Gets the element at an index of the array.
 *
 * If the given index is invalid (out of range), a null pointer is returned.
 *
 * @param array The array you'd like to get an element from.
 * @param index The index of the element you want.
 * @return The element at the designated position, or a null pointer if there isn't any.
 */
void *array_get(array_t *array, size_t index);

/**
 * Sets the element at an index of the array.
 *
 * If there is already an element at that position, the previous element is lost.
 * If the given index is invalid (e.g. if index > size), nothing happens.
 *
 * The caller is responsible from the correct freeing of the inserted element.
 *
 * @param array The array you'd like to set an element of.
 * @param index The index of the element you want to input.
 * @param value The element which should be placed in the array.
 */
void array_set(array_t *array, size_t index, void *value);

/**
 * Add an element to the array. The caller is responsible to free the element.
 *
 * The array will be resized if needed.
 *
 * @param array The array you'd like to add an element to.
 * @param value The value to be added to the array.
 */
void array_add(array_t *array, void *value);

#endif //HEX_ENSEIRB_S6_ARRAY_H
