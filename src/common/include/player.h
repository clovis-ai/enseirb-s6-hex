#ifndef HEX_ENSEIRB_S6_PLAYER_H
#define HEX_ENSEIRB_S6_PLAYER_H

/** @file */

#include <stdlib.h>

#include "move.h"
#include "graph.h"

/**
 * Access to the player's information
 * @return the player name, validating the pattern [a-zA-Z0-9 -_]*
 */
char const* get_player_name();

/**
 * Ask the client to create an opening.
 *
 * The function @code{.c}initialize_graph@endcode must be called before this one.
 *
 * @return A correct move inside the graph.
 */
struct move_t propose_opening();

/**
 * Ask the client if they accept an opening.
 *
 * The function @code{.c}initialize_graph@endcode must be called before this one.
 *
 * @param opening the opening move inside the graph (must be correct!)
 * @return a boolean telling if the player accepts the opening
 */
int accept_opening(const struct move_t opening);

/**
 * Initialize the player's graph.
 *
 * This function can only be called once.
 *
 * @param graph A heap-allocated copy of the graph where the game is played.
 *              Should be freed by the player at the end of the game.
 */
void initialize_graph(struct graph_t* graph);

/**
 * Initialize the player's color.
 *
 * This function can only be called once.
 *
 * @param id The color of the player, either BLACK or WHITE.
 */
void initialize_color(enum color_t id);

/**
 * Computes the next move.
 *
 * @param previous_move The last move made by the other player
 * @return The next move of this player
 */
struct move_t play(struct move_t previous_move);

/**
 *  Announces the end of the game to the player, so he can clean up the memory
 *  he may have been using.
 *
 *  Every allocation during the calls to initialize and player functions must
 *  have been freed.
 */
void finalize();

#endif //HEX_ENSEIRB_S6_PLAYER_H
