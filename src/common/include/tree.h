#ifndef HEX_ENSEIRB_S6_TREE_H
#define HEX_ENSEIRB_S6_TREE_H

/** @file */

#include <stdlib.h>
#include "array.h"

/**
 * A representation of a tree.
 *
 * This tree allows access in O(1) to all its elements.
 *
 * Note that a tree cannot expand.
 */
typedef struct tree tree_t;

/**
 * A representation of a node in a tree.
 */
typedef struct tree_node tree_node_t;

/**
 * Creates a new tree.
 * @param size The size of the tree. Note that you cannot change that later.
 * @return A new tree, that you should free.
 */
tree_t *tree_new(size_t size);

/**
 * Frees a tree, and all of its nodes.
 * @param tree The tree to be freed.
 */
void tree_free(tree_t *tree);

/**
 * Adds a node into the tree.
 * Don't forget to tell which nodes are neighbors, using `tree_node_add_neighbor`.
 *
 * Note that this function will NOT expand the tree size.
 * @param tree The tree you want to add a node to.
 * @param node The node to be added.
 */
void tree_add_node(tree_t *tree, tree_node_t *node);

/**
 * Gets a specific node from the tree.
 * @param tree The tree you want to get a node from.
 * @param id The ID of the node you want.
 * @return The node with that ID if found, otherwise a null pointer.
 */
tree_node_t *tree_get_node(tree_t *tree, size_t id);

/**
 * Creates a new tree node.
 * @param id The ID of this tree node.
 * @param depth The depth of this tree node.
 * @return A new tree node, that you should free.
 */
tree_node_t *tree_node_new(size_t id, unsigned int depth);

/**
 * Frees a tree node.
 * @param node A tree node to be freed.
 */
void tree_node_free(tree_node_t *node);

/**
 * Gets the ID of a tree node.
 * @param node The tree node you want to ID of.
 * @return The ID of the tree node.
 */
size_t tree_node_id(tree_node_t *node);

/**
 * Gets the depth of a tree node.
 * @param node The tree node you want the depth of.
 * @return The depth of the tree node.
 */
unsigned int tree_node_depth(tree_node_t *node);

/**
 * Adds a neighbor to a tree node.
 *
 * Note that this function is NOT mutual: after the call, `node` knows that `neighbor` is its neighbor,
 * but the opposite is not true, `neighbor` does not know that `node` is its neighbor.
 * If that's the behavior you want, call the function twice & swap the parameters.
 *
 * @param node The node the neighbor should be added to.
 * @param neighbor A node that will become a neighbor of the first one.
 */
void tree_node_add_neighbor(tree_node_t *node, tree_node_t *neighbor);

/**
 * Gets the neighbors of this node.
 * @param node The node you want the neighbors of.
 * @return A mutable array of tree_node_t objects.
 */
array_t *tree_node_neighbors(tree_node_t *node);

#endif //HEX_ENSEIRB_S6_TREE_H
