#ifndef HEX_ENSEIRB_S6_QUEUE_H
#define HEX_ENSEIRB_S6_QUEUE_H

/** @file */

/**
 * An implementation of a queue, using two stacks.
 */
typedef struct queue queue_t;

/**
 * Creates a new empty queue.
 * @return A new empty queue, that you should free.
 */
queue_t *queue_new(void);

/**
 * Frees a queue.
 * @param queue The queue to be freed.
 */
void queue_free(queue_t *queue);

/**
 * Pushes an element to a queue.
 * @param queue The queue to be pushed to.
 * @param value The element to add to the queue.
 */
void queue_push(queue_t *queue, void *value);

/**
 * Removes an element from a queue.
 * @param queue The queue to pop from.
 * @return The first element of the queue.
 */
void *queue_pop(queue_t *queue);

/**
 * Is this queue empty?
 * @param queue The queue in question.
 * @return A boolean as to whether the queue is empty or not.
 */
int queue_is_empty(queue_t *queue);

#endif //HEX_ENSEIRB_S6_QUEUE_H
