#include <stdlib.h>
#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <gsl/gsl_spmatrix.h>

const char *mNAME = "manual strategy by 9154";
size_t mNB_VERTICES = 9;

/**
 * Documentation in common/include/graph.h
 */
gsl_spmatrix *mOWNERSHIP = NULL;
int mCOLOR = 2;

/**
 * Correspond to the display function to use
 */
void (*mdisplay)(void) = NULL;

/**
 * Try if the place number m is occupied or not
 * @param m number of the place that would be test
 * @return a boolean telling if the place is free
 */
static int m_occupied(size_t m) {
	return (gsl_spmatrix_get(mOWNERSHIP, 0, m) == 1 || gsl_spmatrix_get(mOWNERSHIP, 1, m) == 1);
}

static void display_sq(void) {
	printf("\n");
	int size = 0;
	while (size * size != (int) mNB_VERTICES)
		size++;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if ((int) gsl_spmatrix_get(mOWNERSHIP, 1, size * i + j) == 0) {
				switch ((int) gsl_spmatrix_get(mOWNERSHIP, 0, size * i + j)) {
					case 0:
						if (size * i + j < 10)
							printf("\033[0m%d  ", size * i + j);
						else
							printf("\033[0m%d ", size * i + j);
						break;
					case 1:
						if (size * i + j < 10)
							printf("\033[31m%d  ", size * i + j);
						else
							printf("\033[31m%d ", size * i + j);

						break;
				}
			} else {
				switch ((int) gsl_spmatrix_get(mOWNERSHIP, 1, size * i + j)) {
					case 0:
						if (size * i + j < 10)
							printf("\033[0m%d  ", size * i + j);
						else
							printf("\033[0m%d ", size * i + j);
						break;
					case 1:
						if (size * i + j < 10)
							printf("\033[34m%d  ", size * i + j);
						else
							printf("\033[34m%d ", size * i + j);
						break;
				}
			}
		}
		printf("\n");
	}
}


static void display_hex(void) {
	printf("\n");
	int size = 0;
	while (size * size != (int) mNB_VERTICES)
		size++;
	for (int i = 0; i < size; i++) {
		for (int k = 0; k <= i; k++) {
			printf("  ");
		}
		for (int j = 0; j < size; j++) {
			if ((int) gsl_spmatrix_get(mOWNERSHIP, 1, size * i + j) == 0) {
				switch ((int) gsl_spmatrix_get(mOWNERSHIP, 0, size * i + j)) {
					case 0:
						if (size * i + j < 10)
							printf("\033[0m%d  ", size * i + j);
						else
							printf("\033[0m%d ", size * i + j);
						break;
					case 1:
						if (size * i + j < 10)
							printf("\033[31m%d  ", size * i + j);
						else
							printf("\033[31m%d ", size * i + j);

						break;
				}
			} else {
				switch ((int) gsl_spmatrix_get(mOWNERSHIP, 1, size * i + j)) {
					case 0:
						if (size * i + j < 10)
							printf("\033[0m%d  ", size * i + j);
						else
							printf("\033[0m%d ", size * i + j);
						break;
					case 1:
						if (size * i + j < 10)
							printf("\033[34m%d  ", size * i + j);
						else
							printf("\033[34m%d ", size * i + j);
						break;
				}
			}
		}
		printf("\n");
	}
}


static size_t ask_user_place(void) {
	mdisplay();
	int place = -1;
	char c[5] = {0};
	while (place < 0 || place > (int) mNB_VERTICES - 1 || m_occupied((size_t) place)) {
		printf("\033[00mPlay a free place on the board, please, you play in %s : ",mCOLOR == 0 ? "red" : "blue");
		scanf("%s", c);
		place = atoi(c);
	}
	return (size_t) place;
}

static int ask_user_opening(struct move_t move) {
	gsl_spmatrix_set(mOWNERSHIP, move.c, move.m, 1);
	mdisplay();
	char c[5] = {0};
	int place = -1;
	while (place != 0 && place != 1) {
		printf("\033[00mAccept (1) or refuse (0) opening \n");
		scanf("%s", c);
		place = atoi(c);
	}
	return (size_t) place;
}


char const *get_player_name() {
	return mNAME;
}

struct move_t propose_opening() {

	int color = 0;

	printf("\n\033[00mPropose an opening :");
	mCOLOR = 0;
	size_t place = ask_user_place();

	struct move_t move = {place, color};

	gsl_spmatrix_set(mOWNERSHIP, 0, place, 1);

	return move;
}

int accept_opening(const struct move_t opening) {
	gsl_spmatrix_set(mOWNERSHIP, 0, opening.m, 1);
	return ask_user_opening(opening);
}

void initialize_graph(struct graph_t *graph) {
	printf("\n\033[00mChoose a type of board : Hexagon (0) or Square (1):\n");
	char c[5] = {0};
	scanf("%s", c);

	if (atoi(c))
		mdisplay = display_sq;
	else
		mdisplay = display_hex;

	mNB_VERTICES = graph->num_vertices;
	mOWNERSHIP = graph->o;
	gsl_spmatrix_free(graph->t);
	free(graph);
}


void initialize_color(enum color_t id) {
	mCOLOR = id;
}

struct move_t play(struct move_t previous_move) {
	if (previous_move.c == 1 || previous_move.c == 0)
		gsl_spmatrix_set(mOWNERSHIP, previous_move.c, previous_move.m, 1);

	printf("\n\033[00mSelect a place to play");
	size_t m = ask_user_place();

	struct move_t move = {m, mCOLOR};
	gsl_spmatrix_set(mOWNERSHIP, mCOLOR, m, 1);

	return move;
}


void finalize() {
	gsl_spmatrix_free(mOWNERSHIP);
}

