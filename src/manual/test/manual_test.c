#include <string.h>
#include <test_suite.h>
#include <player.h>
#include <strategy.h>
#include <dlfcn.h>

int manual_test_name(struct strategy *strategy) {
	test_start("name");

	if (strcmp("manual strategy by 9154", strategy->get_player_name()))
		return test_failure("Not the right name");

	return test_success();
}

int manual_tests(void) {
	test_suite_start("manual");

	int nbr_failures = 0;

	test_start("Loading the strategy");
	struct strategy *manual = load_strategy("cmake-build/src/manual/libzmanual.so");

	if (manual)
		test_success();
	else {
		printf("%s ", dlerror());
		return test_failure("Failed to load the manual library!");
	}

	nbr_failures += manual_test_name(manual);

	test_start("Freeing the strategy");
	free_strategy(manual);
	test_success();

	return nbr_failures;
}
