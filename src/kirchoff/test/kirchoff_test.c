#include <string.h>
#include <test_suite.h>
#include <player.h>
#include <strategy.h>
#include <dlfcn.h>

int kirchoff_test_name(struct strategy *strategy) {
	test_start("name");

	printf("%s ", strategy->get_player_name());
	if (strcmp("Kirchhoff Strategy by 9154", strategy->get_player_name()))
		return test_failure("Not the right name");

	return test_success();
}

int kirchoff_tests(void) {
	test_suite_start("kirchoff");

	int nbr_failures = 0;

	test_start("Loading the strategy");
	struct strategy *manual = load_strategy("cmake-build/src/kirchoff/libkirchoff.so");
	if (manual)
		test_success();
	else {
		printf("%s ", dlerror());
		return test_failure("Failed to load the kirchoff library!");
	}

	nbr_failures += kirchoff_test_name(manual);

	test_start("Freeing the strategy");
	free_strategy(manual);
	test_success();

	return nbr_failures;
}
