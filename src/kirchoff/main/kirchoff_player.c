#include <stdlib.h>
#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <gsl/gsl_spmatrix.h>
#include <tree.h>
#include <edge.h>
#include <gsl/gsl_linalg.h>
#include <kirchhoff_functions.h>

/**
 * Documentation in common/include/graph.h
 */

const char *kNAME = "Kirchhoff Strategy by 9154";
size_t kNB_VERTICES = 0;
struct graph_t *kGRAPH = NULL;
gsl_spmatrix *kOWNERSHIP = NULL;
int kCOLOR = 2;

size_t kM;

vertex_t kVERTEX_FIRST_COMPONENT_BLACK, kVERTEX_SECOND_COMPONENT_BLACK,
		kVERTEX_FIRST_COMPONENT_WHITE, kVERTEX_SECOND_COMPONENT_WHITE;
vertex_t *kALL_VERTICES = NULL;
array_t *kALL_EDGES = NULL;
array_t *kCYCLE_BASIS = NULL;
gsl_spmatrix *kCSR = NULL;

int kDIMINISHED = 0;


void once(const struct graph_t *graph, vertex_t **all_vertices, vertex_t *s1, vertex_t *v1, vertex_t *s2, vertex_t *v2,
          gsl_spmatrix **csr, array_t **all_edges, array_t **cycle_basis) {
	size_t nb_vertices = graph->num_vertices;
	*all_vertices = malloc(nb_vertices * sizeof(vertex_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		(*all_vertices)[i] = i;
	}
	gsl_spmatrix *spm = graph->t;
	*csr = gsl_spmatrix_alloc_nzmax(spm->size1, spm->size2, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(*csr, spm);
	init_components(graph, *csr, s1, v1, s2, v2, *all_vertices);
	*all_edges = list_edges(*csr, *all_vertices);
	size_t *used_edges = calloc(array_size(*all_edges), sizeof(size_t));
	tree_t *tree = spanning_tree(*csr, 0, used_edges, *all_vertices, *all_edges);
	*cycle_basis = finding_cycles(*all_edges, tree, used_edges, nb_vertices, *all_vertices);
	free(used_edges);
	tree_free(tree);
}


double
kirchhoff_resistance(const struct graph_t *graph, vertex_t *all_vertices, const vertex_t s1, const vertex_t v1,
                     const vertex_t s2,
                     const vertex_t v2, const gsl_spmatrix *csr, array_t *all_edges, array_t *cycle_basis) {
	size_t nb_vertices = graph->num_vertices;
	size_t nb_cycles = array_size(all_edges) - nb_vertices + 1;
	gsl_matrix *mat1 = matrix_of_cycles(all_edges, cycle_basis, graph, all_vertices, 0);
	cycle_t *first_team_cycle = generator_cycle(csr, graph, s1, v1, nb_cycles, nb_vertices, all_vertices, 0);
	if (first_team_cycle == NULL) {
		gsl_matrix_free(mat1);
		return -1.;
	}
	gsl_matrix *mat2 = matrix_of_cycles(all_edges, cycle_basis, graph, all_vertices, 1);
	cycle_t *second_team_cycle = generator_cycle(csr, graph, s2, v2, nb_cycles, nb_vertices, all_vertices, 1);
	if (second_team_cycle == NULL) {
		cycle_free(first_team_cycle);
		gsl_matrix_free(mat1);
		gsl_matrix_free(mat2);
		return -2.;
	}
	matrix_with_generator(mat1, all_edges, first_team_cycle, cycle_basis, graph, all_vertices, 0);
	matrix_with_generator(mat2, all_edges, second_team_cycle, cycle_basis, graph, all_vertices, 1);
	cycle_free(first_team_cycle);
	cycle_free(second_team_cycle);
	gsl_vector *vec1 = generator_vector(nb_cycles);
	gsl_vector *vec2 = generator_vector(nb_cycles);
	double solution = solve_for_resistance(mat1, mat2, vec1, vec2, kCOLOR);
	gsl_vector_free(vec1);
	gsl_vector_free(vec2);
	gsl_matrix_free(mat1);
	gsl_matrix_free(mat2);
	return solution;
}


double kirchhoff_resistance_diminished(const struct graph_t *graph, vertex_t *all_vertices, const vertex_t s1,
                                       const vertex_t v1,
                                       const vertex_t s2, const vertex_t v2, const gsl_spmatrix *csr,
                                       array_t *all_edges,
                                       array_t *cycle_basis, enum color_t color) {
	size_t nb_vertices = graph->num_vertices;
	size_t nb_cycles = array_size(all_edges) - nb_vertices + 1;
	gsl_matrix *mat = matrix_of_cycles(all_edges, cycle_basis, graph, all_vertices, color);
	cycle_t *cycle = generator_cycle(csr, graph, color ? s2 : s1, color ? v2 : v1, nb_cycles, nb_vertices,
	                                 all_vertices, color);
	if (cycle == NULL) {
		gsl_matrix_free(mat);
		return -1;
	}
	matrix_with_generator(mat, all_edges, cycle, cycle_basis, graph, all_vertices, color);
	cycle_free(cycle);
	gsl_vector *vec = generator_vector(nb_cycles);
	double solution = solve_for_resistance_diminished(mat, vec);
	gsl_vector_free(vec);
	gsl_matrix_free(mat);
	return solution;
}


/**
 * Try if the place number pos is koccupied or not
 * @param pos number of the place that would be test
 * @return a boolean telling if the place is free
 */
static int koccupied(size_t pos) {
	return (gsl_spmatrix_get(kOWNERSHIP, 0, pos) == 1 || gsl_spmatrix_get(kOWNERSHIP, 1, pos) == 1);
}

/**
 * Try if a place is next to an occupied place
 * @param pos the position to try
 * @return a boolean equals with 1 if there is an occupied place linked to pos, else 0
 */
static int koccupied_neighbours(size_t pos) {
	array_t *neighbours = get_neighbours(kCSR, pos, kALL_VERTICES);
	size_t size = array_size(neighbours);
	for (size_t i = 0; i < size; i++) {
		if (koccupied(* (vertex_t *) array_get(neighbours, i))) {
			array_free(neighbours);
			return 1;
		}
	}
	array_free(neighbours);
	return 0;
}

char const *get_player_name() {
	return kNAME;
}

struct move_t propose_opening() {
	size_t m = 2 * kM - 3;

	while (koccupied(m)) {
		m = rand() % kNB_VERTICES;
	}

	struct move_t move = {m, 0};

	gsl_spmatrix_set(kOWNERSHIP, 0, m, 1);
	return move;
}

int accept_opening(const struct move_t opening) {
	gsl_spmatrix_set(kOWNERSHIP, 0, opening.m, 1);
	if (opening.m % kM == kM - 1 - opening.m /kM)
		return 1;
	return 0;
}

void initialize_graph(struct graph_t *graph) {
	kNB_VERTICES = graph->num_vertices;
	kM = (size_t) sqrt((double) kNB_VERTICES);
	kGRAPH = graph;
	kOWNERSHIP = graph->o;
	once(kGRAPH, &kALL_VERTICES, &kVERTEX_FIRST_COMPONENT_BLACK, &kVERTEX_SECOND_COMPONENT_BLACK,
	     &kVERTEX_FIRST_COMPONENT_WHITE, &kVERTEX_SECOND_COMPONENT_WHITE, &kCSR, &kALL_EDGES, &kCYCLE_BASIS);

}

void initialize_color(enum color_t id) {
	kCOLOR = id;
}

struct move_t play(struct move_t previous_move) {

	gsl_spmatrix_set(kOWNERSHIP, previous_move.c, previous_move.m, 1);

	double max_Kirchhoff = 0.;
	double tmp_Kirchhoff;
	size_t max_vertex;
	if (kDIMINISHED == 0) { //trying to win and make the other lose
		for (size_t i = 0; i < kNB_VERTICES; i++) {
			if (!koccupied(i) && koccupied_neighbours(i)) {
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 1);
				tmp_Kirchhoff = kirchhoff_resistance(kGRAPH, kALL_VERTICES, kVERTEX_FIRST_COMPONENT_BLACK,
				                                     kVERTEX_SECOND_COMPONENT_BLACK, kVERTEX_FIRST_COMPONENT_WHITE,
				                                     kVERTEX_SECOND_COMPONENT_WHITE,
				                                     kCSR, kALL_EDGES, kCYCLE_BASIS);

				if (tmp_Kirchhoff < 0) {
					int side_cant_win = (tmp_Kirchhoff < -1.5);
					if (side_cant_win == kCOLOR) {
						kDIMINISHED = 2;
						max_Kirchhoff = 0;
						gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
						break;
					}
					else {
						max_Kirchhoff = 10000.;
						max_vertex = i;
						gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
						kDIMINISHED = 1;
						break;
					}
				}

				if (tmp_Kirchhoff > max_Kirchhoff) {
					max_Kirchhoff = tmp_Kirchhoff;
					max_vertex = i;
				}
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
			}
		}
	}
	if (kDIMINISHED == 1) { //opponent can't win, trying to finish the game
		for (size_t i = 0; i < kNB_VERTICES; i++) {
			if (!koccupied(i) && koccupied_neighbours(i)) {
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 1);
				tmp_Kirchhoff = kirchhoff_resistance_diminished(kGRAPH, kALL_VERTICES, kVERTEX_FIRST_COMPONENT_BLACK,
				                                                kVERTEX_SECOND_COMPONENT_BLACK,
				                                                kVERTEX_FIRST_COMPONENT_WHITE,
				                                                kVERTEX_SECOND_COMPONENT_WHITE,
				                                                kCSR, kALL_EDGES, kCYCLE_BASIS, kCOLOR);
				if (tmp_Kirchhoff < 0) {
					kDIMINISHED = 3;
					gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
					break;
				}
				if (tmp_Kirchhoff >= max_Kirchhoff) {
					max_Kirchhoff = tmp_Kirchhoff;
					max_vertex = i;
				}
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
			}
		}
	}
	if (kDIMINISHED == 2) { //can't win, tries to make a draw
		for (size_t i = 0; i < kNB_VERTICES; i++) {
			if (!koccupied(i) && koccupied_neighbours(i)) {
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 1);
				tmp_Kirchhoff = kirchhoff_resistance_diminished(kGRAPH, kALL_VERTICES, kVERTEX_FIRST_COMPONENT_BLACK,
				                                                kVERTEX_SECOND_COMPONENT_BLACK,
				                                                kVERTEX_FIRST_COMPONENT_WHITE,
				                                                kVERTEX_SECOND_COMPONENT_WHITE,
				                                                kCSR, kALL_EDGES, kCYCLE_BASIS, !kCOLOR);
				if (tmp_Kirchhoff < 0) {
					kDIMINISHED = 3;
					gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
					break;
				}
				if (tmp_Kirchhoff <= max_Kirchhoff) {
					max_Kirchhoff = tmp_Kirchhoff;
					max_vertex = i;
				}
				gsl_spmatrix_set(kOWNERSHIP, kCOLOR, i, 0);
			}
		}
	}
	if (kDIMINISHED == 3) { //nobody can win, just finish the game
		max_vertex = rand() % kNB_VERTICES;
			while (koccupied(max_vertex))
				max_vertex = rand() % kNB_VERTICES;

	}
	struct move_t move = {max_vertex, kCOLOR};
	gsl_spmatrix_set(kOWNERSHIP, kCOLOR, max_vertex, 1);

	return move;
}

void finalize() {
	free_graph(kGRAPH);
	free(kALL_VERTICES);
	free_list_edges(kALL_EDGES);
	gsl_spmatrix_free(kCSR);
	free_cycle_basis(kCYCLE_BASIS);
}
