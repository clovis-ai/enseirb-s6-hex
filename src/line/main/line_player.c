#include <stdlib.h>
#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <time.h>
#include <gsl/gsl_spmatrix.h>


const char *lNAME = "line player by 9154";
size_t lNB_VERTICES = 9;

/**
 * Last played place
 */
size_t lMEM = 0;

/**
 * Documentation in common/include/graph.h
 */
gsl_spmatrix *lOWNERSHIP = NULL;
gsl_spmatrix *lADJACENCY = NULL;
int lCOLOR = 2;

/**
 * Try if the place number m is occupied or not
 * @param m number of the place that would be test
 * @return a boolean telling if the place is free
 */
static int loccupied(size_t m) {
	return (gsl_spmatrix_get(lOWNERSHIP, 0, m) == 1 || gsl_spmatrix_get(lOWNERSHIP, 1, m) == 1);
}


char const *get_player_name() {
	return lNAME;
}

struct move_t propose_opening() {
	size_t m = rand() % lNB_VERTICES;

	if (lOWNERSHIP != NULL) {
		while (loccupied(m))
			m = rand() % lNB_VERTICES;
	}

	struct move_t move = {m, 0};

	if (lOWNERSHIP != NULL)
		gsl_spmatrix_set(lOWNERSHIP, 0, m, 1);

	return move;
}

int accept_opening(const struct move_t opening) {
	if (lOWNERSHIP != NULL)
		gsl_spmatrix_set(lOWNERSHIP, 0, opening.m, 1);

	return 0;
}

void initialize_graph(struct graph_t *graph) {
	lNB_VERTICES = graph->num_vertices;
	lMEM = lNB_VERTICES;
	lOWNERSHIP = graph->o;
	lADJACENCY = graph->t;
	free(graph);
}

void initialize_color(enum color_t id) {
	lCOLOR = id;
}

struct move_t play(struct move_t previous_move) {
	if (previous_move.c == 1 || previous_move.c == 0)
		gsl_spmatrix_set(lOWNERSHIP, previous_move.c, previous_move.m, 1);

	size_t m = lNB_VERTICES;

	int lNEXT_SIZE = 0;
	size_t lNEXT[6];
	size_t rand_val;

	while(lNEXT_SIZE == 0) {

		while (lMEM == lNB_VERTICES) {
			rand_val = rand() % lNB_VERTICES;
			if (gsl_spmatrix_get(lOWNERSHIP, lCOLOR, rand_val))
				lMEM = rand_val;
		}


		for (size_t i = 0; i < lNB_VERTICES; i++) {
			if (gsl_spmatrix_get(lADJACENCY, i, lMEM) && !loccupied(i)) {
				lNEXT[lNEXT_SIZE] = i;
				lNEXT_SIZE++;
			}
		}
		lMEM = lNB_VERTICES;
	}

	m = lNEXT[rand() % lNEXT_SIZE];
	lMEM = m;
	gsl_spmatrix_set(lOWNERSHIP, lCOLOR, m, 1);

	struct move_t move = {m, lCOLOR};

	return move;
}

void finalize() {
	gsl_spmatrix_free(lOWNERSHIP);
	gsl_spmatrix_free(lADJACENCY);
}
