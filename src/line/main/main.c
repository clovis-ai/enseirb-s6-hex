#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <common.h>

int main() {
    printf("Hello World, from the line strategy!\n");
    common_say_hello();
    return 0;
}
