#include <stdio.h>
#include <graph.h>
#include <move.h>
#include <string.h>
#include <common.h>
#include <test_suite.h>
#include <player.h>
#include <stdlib.h>
#include <gsl/gsl_spmatrix.h>
#include <strategy.h>
#include <dlfcn.h>


int line_test_name(struct strategy *strategy) {
	test_start("name");

	if (strcmp("line player by 9154", strategy->get_player_name()))
		return test_failure("Not the right name");

	return test_success();
}


int line_test_propose_opening_different(struct strategy *strategy) {
	test_start("propose opening correctly");

	size_t k = 20;

	struct graph_t *graph = malloc(sizeof(struct graph_t));
	graph->t = gsl_spmatrix_alloc(k, k);
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->num_vertices = k;

	gsl_spmatrix_set(graph->o, 1, 1, 1);
	gsl_spmatrix_set(graph->o, 0, 2, 1);
	gsl_spmatrix_set(graph->o, 0, 5, 1);
	gsl_spmatrix_set(graph->o, 1, 15, 1);

	strategy->initialize_graph(graph);

	struct move_t move = strategy->propose_opening();

	if (move.c >= 2 || move.c < 0 ){
		gsl_spmatrix_free(graph->t);
		gsl_spmatrix_free(graph->o);
		free(graph);
		return test_failure("Incorrect color");
	}

	if (move.m == 1 || move.m == 2 || move.m == 5 || move.m == 15){
		gsl_spmatrix_free(graph->t);
		gsl_spmatrix_free(graph->o);
		free(graph);
		return test_failure("Incorrect location");
	}

	strategy->finalize();
	return test_success();
}

int line_test_accept_opening(struct strategy *strategy) {
	test_start("accept opening");

	struct move_t move = {1, 2};

	int rep = strategy->accept_opening(move);
	(void)rep;

	return test_success();
}

int line_test_play_case_1(struct strategy *strategy) {
	test_start("play a place case 1");

	size_t k = 25;

	struct graph_t *graph = malloc(sizeof(struct graph_t));
	graph->t = gsl_spmatrix_alloc(k, k);
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->num_vertices = k;

	gsl_spmatrix_set(graph->t, 2, 7, 1);
	gsl_spmatrix_set(graph->t, 3, 7, 1);
	gsl_spmatrix_set(graph->t, 6, 7, 1);
	gsl_spmatrix_set(graph->t, 8, 7, 1);
	gsl_spmatrix_set(graph->t, 11, 7, 1);
	gsl_spmatrix_set(graph->t, 12, 7, 1);
	gsl_spmatrix_set(graph->t, 7, 2, 1);
	gsl_spmatrix_set(graph->t, 7, 3, 1);
	gsl_spmatrix_set(graph->t, 7, 6, 1);
	gsl_spmatrix_set(graph->t, 7, 8, 1);
	gsl_spmatrix_set(graph->t, 7, 11, 1);
	gsl_spmatrix_set(graph->t, 7, 12, 1);

	gsl_spmatrix_set(graph->t, 6, 11, 1);
	gsl_spmatrix_set(graph->t, 7, 11, 1);
	gsl_spmatrix_set(graph->t, 10, 11, 1);
	gsl_spmatrix_set(graph->t, 12, 11, 1);
	gsl_spmatrix_set(graph->t, 15, 11, 1);
	gsl_spmatrix_set(graph->t, 16, 11, 1);
	gsl_spmatrix_set(graph->t, 11, 6, 1);
	gsl_spmatrix_set(graph->t, 11, 7, 1);
	gsl_spmatrix_set(graph->t, 11, 10, 1);
	gsl_spmatrix_set(graph->t, 11, 12, 1);
	gsl_spmatrix_set(graph->t, 11, 15, 1);
	gsl_spmatrix_set(graph->t, 11, 16, 1);

	gsl_spmatrix_set(graph->o, 0, 0, 1);
	gsl_spmatrix_set(graph->o, 0, 1, 1);
	gsl_spmatrix_set(graph->o, 0, 2, 1);
	gsl_spmatrix_set(graph->o, 0, 3, 1);
	gsl_spmatrix_set(graph->o, 0, 21, 1);
	gsl_spmatrix_set(graph->o, 0, 22, 1);
	gsl_spmatrix_set(graph->o, 0, 23, 1);
	gsl_spmatrix_set(graph->o, 0, 24, 1);

	gsl_spmatrix_set(graph->o, 1, 5, 1);
	gsl_spmatrix_set(graph->o, 1, 10, 1);
	gsl_spmatrix_set(graph->o, 1, 15, 1);
	gsl_spmatrix_set(graph->o, 1, 20, 1);
	gsl_spmatrix_set(graph->o, 1, 4, 1);
	gsl_spmatrix_set(graph->o, 1, 9, 1);
	gsl_spmatrix_set(graph->o, 1, 14, 1);
	gsl_spmatrix_set(graph->o, 1, 19, 1);

	strategy->initialize_graph(graph);
	strategy->initialize_color(0);

	struct move_t move = {16, 1};

	move = strategy->play(move);

	if (move.c != 0){
		strategy->finalize();
		return test_failure("Play with the wrong color");
	}

	if (move.m != 7){
		strategy->finalize();
		return test_failure("Play the wrong place");
	}

	move.c = 1;
	move.m = 17;

	move = strategy->play(move);

	if (move.c != 0){
		strategy->finalize();
		return test_failure("Play with the wrong color the second time");
	}

	if (move.m != 6 && move.m != 8 && move.m != 11 && move.m != 12){
		strategy->finalize();
		return test_failure("Play the wrong place the second time");
	}

	strategy->finalize();
	return test_success();
}



int line_tests(void) {
	test_suite_start("line");

	int nbr_failures = 0;

	test_start("Loading the strategy");
	struct strategy *line = load_strategy("cmake-build/src/line/libline.so");
	if (line)
		test_success();
	else {
		printf("%s ", dlerror());
		return test_failure("Failed to load the line library!");
	}

	printf("Loaded %p, %s\n", line->dl_handle, line->get_player_name());

	nbr_failures += line_test_name(line);
	nbr_failures += line_test_propose_opening_different(line);
	nbr_failures += line_test_play_case_1(line);


	test_start("Freeing the strategy");
	free_strategy(line);
	test_success();

	return nbr_failures;
}

  

