#ifndef HEX_ENSEIRB_S6_GAME_LOOP_H
#define HEX_ENSEIRB_S6_GAME_LOOP_H

/** @file */

#include "strategy.h"

int
game_loop(const struct strategy *strat1, const struct strategy *strat2, const size_t graph_size,
          const char graph_shape);

#endif //HEX_ENSEIRB_S6_GAME_LOOP_H
