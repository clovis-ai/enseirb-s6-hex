#ifndef HEX_ENSEIRB_S6_GRAPH_INIT_H
#define HEX_ENSEIRB_S6_GRAPH_INIT_H

/** @file */

#include "graph.h"
#include <gsl/gsl_spmatrix.h>

/**
 * Generates t the necessary game board
 * @param m width of one side of the game board
 * @param t represents the type of graph h for hexagonal, c for square, and t for triangle
 * @return the adjacency graph needed
 */
gsl_spmatrix *generate_graph_adjacency(size_t m, char t);

/**
 * Generates nb_vertices of game board
 * @param m width of one side of the game board
 * @param t represents the type of graph h for hexagonal, c for square, and t for triangle
 * @return the total number of vertices inside the described graph
 */
size_t generate_nb_vertices(size_t m, char t);

/**
 * Generates o at the beginning the ownership of the game before it starts
 * @param m width of one side of the game board
 * @param t t represents the type of graph h for hexagonal, c for square, and t for triangle
 * @return the adjacency graph at the beginning of the game
 */
gsl_spmatrix *generate_graph_ownership(size_t m, char t);

/**
 * Generates a graph g empty
 * @param m width of one side of the game board
 * @param t t represents the type of graph h for hexagonal, c for square, and t for triangle
 * @return the adjacency graph at the beginning of the game
 */
struct graph_t *generate_empty_graph(size_t m, char t);

/**
 * Creates the board. The caller has the responsibility to free the returned object.
 * @param graph_size The width of one side of the game board.
 * @param graph_shape The shape of the board. 'h' for hexagonal, 'c' for square, 't' for triangle.
 * @return An initialized graph.
 */
struct graph_t *init_graph(size_t graph_size, char graph_shape);

#endif //HEX_ENSEIRB_S6_GRAPH_INIT_H
