#include "game_loop.h"
#include <stdlib.h>
#include <gsl/gsl_spmatrix.h>
#include "graph_init.h"
#include <stdio.h>


void symmetric_set_adjacency_spmatrix(gsl_spmatrix *spm, size_t i, size_t j, size_t nb_vertices) {
	double one = 1;
	gsl_spmatrix_set(spm, i, j, one); //if a vertex i is connected to another vertex j, j is also connected to i
	gsl_spmatrix_set(spm, j, i, one); //therefore, these first two lines
	gsl_spmatrix_set(spm, nb_vertices - 1 - j, nb_vertices - 1 - i, one); //every graph is symmetrical by its center
	gsl_spmatrix_set(spm, nb_vertices - 1 - i, nb_vertices - 1 - j, one); //therefore, these two lines
}

void symmetric_set_ownership_spmatrix(gsl_spmatrix *spm, size_t n_player, size_t i, size_t nb_vertices) {
	double one = 1;
	gsl_spmatrix_set(spm, n_player, i, one); //this time only using the symmetry by the center but leaving the choice
	gsl_spmatrix_set(spm, n_player, nb_vertices - 1 - i, one); //of the player in the form of the line
}

gsl_spmatrix *generate_square_adjacency(const size_t m) {
	size_t nb_vertices = m * m;
	gsl_spmatrix *spm = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, 4 * nb_vertices, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm); //sets all value that won't be set here to 0
	for (size_t i = 0; i < m; i++) {
		for (size_t j = 0; j < m; j++) {
			if (j != 0) //if j == 0 you can't go backward
				symmetric_set_adjacency_spmatrix(spm, i * m + j, i * m + j - 1, nb_vertices);
			if (i != 0) //if i == 0 you can't go upward
				symmetric_set_adjacency_spmatrix(spm, i * m + j, (i - 1) * m + j, nb_vertices);
		}
	}
	return spm;
}

gsl_spmatrix *generate_square_hex_ownership(size_t m) {
	size_t nb_vertices = m * m;
	gsl_spmatrix *spm = gsl_spmatrix_alloc_nzmax(2, nb_vertices, nb_vertices, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm);
	for (size_t i = 0; i < m - 1; i++) {
		symmetric_set_ownership_spmatrix(spm, 0, i, nb_vertices); //covering the top and bottom side
		symmetric_set_ownership_spmatrix(spm, 1, (i + 1) * m, nb_vertices); //covering the left and right side
	}
	return spm;
}

gsl_spmatrix *generate_hex_adjacency(size_t m) {
	size_t nb_vertices = m * m;
	gsl_spmatrix *spm = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, 6 * nb_vertices, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm);
	for (size_t i = 0; i < m; i++) {
		for (size_t j = 0; j < m; j++) {
			if (j != 0)
				symmetric_set_adjacency_spmatrix(spm, i * m + j, i * m + j - 1, nb_vertices);
			if (i != 0)
				symmetric_set_adjacency_spmatrix(spm, i * m + j, (i - 1) * m + j, nb_vertices);
			if ((i != 0) && (j != m - 1))
				symmetric_set_adjacency_spmatrix(spm, i * m + j, (i - 1) * m + j + 1, nb_vertices); //covers an extra
			//possibility in contrast with square as you can go up right (and down left (by symmetry))
			//though, you can only do so if you are not in the first line (i!=0) and not in the last column (j!=m-1)
		}
	}
	return spm;
}

gsl_spmatrix *generate_triangle_adjacency(size_t m) { //by far the most complex graph that we will deal with in 2 parts
	//notice that in this graph, the vertices are still numbered by lines
	size_t nb_vertices = 6 * m * m;
	gsl_spmatrix *spm = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, 3 * nb_vertices, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm);
	for (size_t i = 0; i < m - 1; i++) { //first part is the top and bottom of the hexagonal shape
		for (size_t j = 0; j < (i + 1) * 2; j++) { //they are (i + 1) * 2 vertices per line during this part
			//we stop before line m - 1 which is the first one being regular in its number of vertices
			symmetric_set_adjacency_spmatrix(spm, i * (i + 1) + j, (i + 1) * (i + 2) + j + 1, nb_vertices);
			if (j % 2 == 0)
				symmetric_set_adjacency_spmatrix(spm, i * (i + 1) + j, i * (i + 1) + j + 1, nb_vertices);
		}
	}
	size_t gap = (m - 1) * m; //this accounts for the number of vertices already dealt with
	for (size_t i = 0; i < m + 1; i++) { //second part is middle height and is way more regular (every line contains 2 * m)
		for (size_t j = 0; j < 2 * m; j++) {
			symmetric_set_adjacency_spmatrix(spm, gap + i * 2 * m + j, gap + (i + 1) * 2 * m + j, nb_vertices);
			if (((i + j) % 2 == 0) && (j != 2 * m - 1))
				symmetric_set_adjacency_spmatrix(spm, gap + i * 2 * m + j, gap + i * 2 * m + j + 1, nb_vertices);
		}
	}
	return spm;
}

gsl_spmatrix *generate_triangle_ownership(size_t m) {
	size_t nb_vertices = 6 * m * m;
	gsl_spmatrix *spm = gsl_spmatrix_alloc_nzmax(2, nb_vertices, nb_vertices, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm);
	size_t n_player = 0;
	for (size_t i = 0; i < m; i++) {
		symmetric_set_ownership_spmatrix(spm, 0, i * (i + 1), nb_vertices); //sets the top left (and bot right)
		symmetric_set_ownership_spmatrix(spm, 0, i * (i + 1) + 1, nb_vertices); //sides to first player
		if (i == m / 2)
			n_player = 1; //for the top right side, there is a vertix that is not given to anyone when i == m / 2
		//I use this vertex as the swapping point (the moment where I stop giving to the first player but to the secund)
		else
			symmetric_set_ownership_spmatrix(spm, n_player, i * (i + 3), nb_vertices); //the exception
		symmetric_set_ownership_spmatrix(spm, n_player, i * (i + 3) + 1, nb_vertices); //extreme top right side
	}
	size_t gap = m * (m + 1);
	for (size_t i = 0; i < 2 * m - 1; i++) //covers the left and right side of the middle in height
		symmetric_set_ownership_spmatrix(spm, 1, gap + i * 2 * m, nb_vertices);
	return spm;
}

gsl_spmatrix *generate_graph_adjacency(size_t m, char t) {
	gsl_spmatrix *spm;
	switch (t) {
	case 'c':
		spm = generate_square_adjacency(m);
		break;
	case 'h':
		spm = generate_hex_adjacency(m);
		break;
	case 't':
		if (m % 2 == 1) {
			printf("Forbidden to generate triangular graph of odd width\n");
			exit(1);
		}
		spm = generate_triangle_adjacency(m);
		break;
	default:
		printf("Variable t given to adjacency should be either char c, h or t\n");
		exit(1);
	}
	return spm;
}

size_t generate_nb_vertices(size_t m, char t) {
	size_t nb_vertices;
	switch (t) {
	case 'c':
	case 'h':
		nb_vertices = m * m;
		break;
	case 't':
		nb_vertices = 6 * m * m;
		break;
	default:
		printf("Variable t given to nb_vertices should be either char c, h or t\n");
		exit(1);
	}
	return nb_vertices;
}

gsl_spmatrix *generate_graph_ownership(size_t m, char t) {
	gsl_spmatrix *spm;
	switch (t) {
	case 'c':
	case 'h':
		spm = generate_square_hex_ownership(m);
		break;
	case 't':
		if (m % 2 == 1) {
			printf("Forbidden to generate triangular ownership of odd width\n");
			exit(1);
		}
		spm = generate_triangle_ownership(m);
		break;
	default:
		printf("Variable t given to ownership should be either char c, h or t\n");
		exit(1);
	}
	return spm;
}

struct graph_t *generate_empty_graph(size_t m, char t)
{
	struct graph_t *g = malloc(sizeof(struct graph_t));
	g->num_vertices = generate_nb_vertices(m, t);
	g->t = generate_graph_adjacency(m, t);
	g->o = generate_graph_ownership(m, t);
	return g;
}

struct graph_t *init_graph(const size_t graph_size, const char graph_shape) {
	struct graph_t *graph = malloc(sizeof(*graph));
	graph->num_vertices = generate_nb_vertices(graph_size, graph_shape);
	graph->t = generate_graph_adjacency(graph_size, graph_shape);
	graph->o = generate_graph_ownership(graph_size, graph_shape);
	return graph;
}
