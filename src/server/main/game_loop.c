#include <common.h>
#include "game_loop.h"
#include "graph_init.h"
#include <is_winning.h>
#include <cycle.h>

static void print_move(struct move_t *move, char *eol) {
	printf("%zu by %s%s", move->m, move->c == WHITE ? "white" : "black", eol);
}

int game_loop(const struct strategy *strat1, const struct strategy *strat2, const size_t graph_size,
              const char graph_shape) {
	struct graph_t *graph = init_graph(graph_size, graph_shape);
	struct graph_t *graph1 = copy_graph(graph);
	struct graph_t *graph2 = copy_graph(graph);
	strat1->initialize_graph(graph1);
	strat2->initialize_graph(graph2);

	gsl_spmatrix *spm = graph->t;
	size_t nb_vertices = spm->size1;
	size_t *all_vertices = malloc(nb_vertices * sizeof(size_t));
	for (vertex_t i = 0; i < nb_vertices; i++) {
		all_vertices[i] = i;
	}
	gsl_spmatrix *csr = gsl_spmatrix_alloc_nzmax(nb_vertices, nb_vertices, spm->nz, GSL_SPMATRIX_CSR);
	gsl_spmatrix_csr(csr, spm);

	struct move_t proposition = strat1->propose_opening();
	printf("“%s” (%p) has proposed the move: ", strat1->get_player_name(), strat1->dl_handle);
	print_move(&proposition, ".\n");

	const struct strategy *player_white, *player_black;
	if (strat2->accept_opening(proposition)) {
		printf("“%s” (%p) has accepted.\n", strat2->get_player_name(), strat2->dl_handle);
		player_white = strat1;
		player_black = strat2;
	} else {
		printf("“%s” (%p) has refused.\n", strat2->get_player_name(), strat2->dl_handle);
		player_black = strat1;
		player_white = strat2;
	}
	player_white->initialize_color(WHITE);
	player_black->initialize_color(BLACK);
	printf("White is “%s” (%p).\n", player_white->get_player_name(), player_white->dl_handle);
	printf("Black is “%s” (%p).\n", player_black->get_player_name(), player_black->dl_handle);

	const struct strategy *player_now = player_white;
	struct move_t last_move = proposition;
	apply_move(&proposition, graph);
	int winner = -1;
	is_winning_init(graph, csr, all_vertices);

	do {
		printf("\n%s to play.\n", player_now == player_white ? "White" : "Black");
		struct move_t move = player_now->play(last_move);
		printf("Selected move: ");
		print_move(&move, "\n");

		if (is_valid(&move, player_now == player_white ? WHITE : BLACK, graph)) {
			printf("The move is valid.\n");
			apply_move(&move, graph);
			last_move = move;
		} else {
			printf("The move is invalid.\n");
			winner = player_now == strat1 ? 2 : 1;
			break;
		}
		winner = is_winning(graph, csr, all_vertices);

		player_now = player_now == player_white ? player_black : player_white;
	} while (!winner);
	gsl_spmatrix_free(graph->o);
	gsl_spmatrix_free(graph->t);
	free(graph);

	free(all_vertices);
	gsl_spmatrix_free(csr);

	player_white->finalize();
	player_black->finalize();
	return winner;
}
