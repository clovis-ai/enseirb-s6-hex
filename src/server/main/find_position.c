#include <unistd.h>
#include <stdio.h>
#include "find_position.h"

#ifndef PATH_BUFFER_SIZE
#define PATH_BUFFER_SIZE 100
#endif

void print_local_path(void) {
	char buffer[PATH_BUFFER_SIZE];
	getcwd(buffer, PATH_BUFFER_SIZE);

	printf("%s ", buffer);
}
