#include <stdio.h>
#include <string.h>
#include <common.h>
#include <dlfcn.h>
#include "strategy.h"
#include "find_position.h"
#include "game_loop.h"

struct cli_options {
	int boardSize;
	char boardShape;
	char *player1;
	char *player2;
};

void process_cli(int argc, char *argv[], struct cli_options *options) {
	options->boardSize = 10;
	options->boardShape = 'c';
	options->player1 = 0;
	options->player2 = 0;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-m") == 0) {
			if (i >= argc) {
				printf("Option -m expects an integer.\n");
			} else {
				options->boardSize = atoi(argv[i + 1]);
				if (options->boardSize <= 2 || options->boardSize >= 1000) {
					printf("The board size should be between 2 and 1000: %d.\n", options->boardSize);
					options->boardSize = 10;
				}
				printf("The board size will be %d.\n", options->boardSize);
				i++;
			}
		} else if (strcmp(argv[i], "-t") == 0) {
			if (i >= argc) {
				printf("Option -t expects either 't', 'c' or 'h'.\n");
			} else {
				options->boardShape = argv[i + 1][0];
				if (options->boardShape != 'c' && options->boardShape != 't' && options->boardShape != 'h') {
					printf("The board shape should be either 'c', 't' or 'h': %c\n", options->boardShape);
					options->boardShape = 'c';
				}
				printf("The board shape will be %c.\n", options->boardShape);
				i++;
			}
		} else if (options->player1 == 0) {
			printf("Player 1 will be loaded from %s\n", argv[i]);
			options->player1 = argv[i];
		} else if (options->player2 == 0) {
			printf("Player 2 will be loaded from %s\n", argv[i]);
			options->player2 = argv[i];
		} else {
			printf("Unknown option: %s\n", argv[i]);
		}
	}

	if (!options->player1) {
		printf("No first player was specified! You should specify a .so file.\n");
		exit(1);
	} else if (!options->player2) {
		printf("No second player was specified! You should specify a .so file.\n");
		exit(1);
	}
}

int main(int argc, char *argv[]) {
	printf("Starting the Hex game, coded by Baptiste Benard, Ivan Canet, Raphaël Comet, and Tom Moenne-Loccoz!\n");
	common_say_hello();
	printf("Running from: ");
	print_local_path();
	printf("\n");

	struct cli_options options;
	process_cli(argc, argv, &options);

	printf("\nLoading strategy %s... ", options.player1);
	struct strategy *player1 = load_strategy(options.player1);
	if (player1 == 0) {
		printf("Could not load the strategy: %s\n", dlerror());
		return 2;
	}
	printf("Loaded as %p, “%s”.\n", player1->dl_handle, player1->get_player_name());

	printf("Loading strategy %s... ", options.player2);
	struct strategy *player2 = load_strategy(options.player2);
	if (player2 == 0) {
		printf("Could not load the strategy: %s\n", dlerror());
		return 3;
	}
	printf("Loaded as %p, “%s”.\n", player2->dl_handle, player2->get_player_name());

	printf("\nThe game is starting now...\n");
	int winner = game_loop(player1, player2, options.boardSize, options.boardShape);

	if (winner == 1)
		printf("\nStrategy %p, “%s” has won!\n", player1->dl_handle, player1->get_player_name());
	else if (winner == 2)
		printf("\nStrategy %p, “%s” has won!\n", player2->dl_handle, player2->get_player_name());
	else if (winner == 3)
		printf("\nThere is a draw\n");
	else
		printf("\nSomething went wrong, there is no winner.\n");

	printf("\nThe game has ended.\n");
	printf("Freeing strategy 1...\n");
	free_strategy(player1);
	printf("Freeing strategy 2...\n");
	free_strategy(player2);
	printf("Done.\n");

	return 0;
}
