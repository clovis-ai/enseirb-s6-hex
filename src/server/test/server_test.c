#include <stdlib.h>
#include <test_suite.h>
#include <gsl/gsl_spmatrix.h>
#include "../main/graph_init.h"
#include <dlfcn.h>
#include "strategy.h"
#include "../main/find_position.h"

int test_local_path() {
	test_start("local_path");

	print_local_path();

	return test_success();
}

int server_test_square_adjacency(void) {
	test_start("Square adjacency");
	gsl_spmatrix *spm1 = generate_graph_adjacency(3, 'c');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(9, 9, 24, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 1, 0, one);
	gsl_spmatrix_set(spm2, 1, 2, one);
	gsl_spmatrix_set(spm2, 1, 4, one);
	gsl_spmatrix_set(spm2, 2, 1, one);
	gsl_spmatrix_set(spm2, 2, 5, one);
	gsl_spmatrix_set(spm2, 3, 0, one);
	gsl_spmatrix_set(spm2, 3, 4, one);
	gsl_spmatrix_set(spm2, 3, 6, one);
	gsl_spmatrix_set(spm2, 4, 1, one);
	gsl_spmatrix_set(spm2, 4, 3, one);
	gsl_spmatrix_set(spm2, 4, 5, one);
	gsl_spmatrix_set(spm2, 4, 7, one);
	gsl_spmatrix_set(spm2, 5, 2, one);
	gsl_spmatrix_set(spm2, 5, 4, one);
	gsl_spmatrix_set(spm2, 5, 8, one);
	gsl_spmatrix_set(spm2, 6, 3, one);
	gsl_spmatrix_set(spm2, 6, 7, one);
	gsl_spmatrix_set(spm2, 7, 4, one);
	gsl_spmatrix_set(spm2, 7, 6, one);
	gsl_spmatrix_set(spm2, 7, 8, one);
	gsl_spmatrix_set(spm2, 8, 5, one);
	gsl_spmatrix_set(spm2, 8, 7, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a 3*3 square adjacency matrix");
}

int server_test_hex_adjacency(void) {
	test_start("Hex adjacency");
	gsl_spmatrix *spm1 = generate_graph_adjacency(3, 'h');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(9, 9, 32, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 1, 0, one);
	gsl_spmatrix_set(spm2, 1, 2, one);
	gsl_spmatrix_set(spm2, 1, 3, one);
	gsl_spmatrix_set(spm2, 1, 4, one);
	gsl_spmatrix_set(spm2, 2, 1, one);
	gsl_spmatrix_set(spm2, 2, 4, one);
	gsl_spmatrix_set(spm2, 2, 5, one);
	gsl_spmatrix_set(spm2, 3, 0, one);
	gsl_spmatrix_set(spm2, 3, 1, one);
	gsl_spmatrix_set(spm2, 3, 4, one);
	gsl_spmatrix_set(spm2, 3, 6, one);
	gsl_spmatrix_set(spm2, 4, 1, one);
	gsl_spmatrix_set(spm2, 4, 2, one);
	gsl_spmatrix_set(spm2, 4, 3, one);
	gsl_spmatrix_set(spm2, 4, 5, one);
	gsl_spmatrix_set(spm2, 4, 6, one);
	gsl_spmatrix_set(spm2, 4, 7, one);
	gsl_spmatrix_set(spm2, 5, 2, one);
	gsl_spmatrix_set(spm2, 5, 4, one);
	gsl_spmatrix_set(spm2, 5, 7, one);
	gsl_spmatrix_set(spm2, 5, 8, one);
	gsl_spmatrix_set(spm2, 6, 3, one);
	gsl_spmatrix_set(spm2, 6, 4, one);
	gsl_spmatrix_set(spm2, 6, 7, one);
	gsl_spmatrix_set(spm2, 7, 4, one);
	gsl_spmatrix_set(spm2, 7, 5, one);
	gsl_spmatrix_set(spm2, 7, 6, one);
	gsl_spmatrix_set(spm2, 7, 8, one);
	gsl_spmatrix_set(spm2, 8, 5, one);
	gsl_spmatrix_set(spm2, 8, 7, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a 3*3 hex adjacency matrix");
}

int server_test_hex_adjacency_incomplete(void) {
	test_start("Hex adjacency incomplete");
	gsl_spmatrix *spm = generate_graph_adjacency(6, 'h');

	if (!gsl_spmatrix_get(spm, 21, 20) || !gsl_spmatrix_get(spm, 20, 21)\
 || !gsl_spmatrix_get(spm, 25, 20) || !gsl_spmatrix_get(spm, 20, 25)\
 || !gsl_spmatrix_get(spm, 26, 20) || !gsl_spmatrix_get(spm, 20, 26)\
 || !gsl_spmatrix_get(spm, 19, 20) || !gsl_spmatrix_get(spm, 20, 19)\
 || !gsl_spmatrix_get(spm, 14, 20) || !gsl_spmatrix_get(spm, 20, 14)\
 || !gsl_spmatrix_get(spm, 15, 20) || !gsl_spmatrix_get(spm, 20, 15)) {
		gsl_spmatrix_free(spm);
		return test_failure("20 not well linked");
	}

	if (!gsl_spmatrix_get(spm, 21, 15) || !gsl_spmatrix_get(spm, 15, 21)\
 || !gsl_spmatrix_get(spm, 20, 15) || !gsl_spmatrix_get(spm, 15, 20)\
 || !gsl_spmatrix_get(spm, 16, 15) || !gsl_spmatrix_get(spm, 15, 16)\
 || !gsl_spmatrix_get(spm, 9, 15) || !gsl_spmatrix_get(spm, 15, 9)\
 || !gsl_spmatrix_get(spm, 14, 15) || !gsl_spmatrix_get(spm, 15, 14)\
 || !gsl_spmatrix_get(spm, 10, 15) || !gsl_spmatrix_get(spm, 15, 10)) {
		gsl_spmatrix_free(spm);
		return test_failure("15 not well linked");
	}

	if (!gsl_spmatrix_get(spm, 20, 21) || !gsl_spmatrix_get(spm, 21, 20)\
 || !gsl_spmatrix_get(spm, 27, 21) || !gsl_spmatrix_get(spm, 21, 27)\
 || !gsl_spmatrix_get(spm, 26, 21) || !gsl_spmatrix_get(spm, 21, 26)\
 || !gsl_spmatrix_get(spm, 22, 21) || !gsl_spmatrix_get(spm, 21, 22)\
 || !gsl_spmatrix_get(spm, 16, 21) || !gsl_spmatrix_get(spm, 21, 16)\
 || !gsl_spmatrix_get(spm, 15, 21) || !gsl_spmatrix_get(spm, 21, 15)) {
		gsl_spmatrix_free(spm);
		return test_failure("21 not well linked");
	}

	if (!gsl_spmatrix_get(spm, 20, 26) || !gsl_spmatrix_get(spm, 26, 20)\
 || !gsl_spmatrix_get(spm, 27, 26) || !gsl_spmatrix_get(spm, 26, 27)\
 || !gsl_spmatrix_get(spm, 21, 26) || !gsl_spmatrix_get(spm, 26, 21)\
 || !gsl_spmatrix_get(spm, 25, 26) || !gsl_spmatrix_get(spm, 26, 25)\
 || !gsl_spmatrix_get(spm, 31, 26) || !gsl_spmatrix_get(spm, 26, 31)\
 || !gsl_spmatrix_get(spm, 32, 26) || !gsl_spmatrix_get(spm, 26, 32)) {
		gsl_spmatrix_free(spm);
		return test_failure("26 not well linked");
	}

	gsl_spmatrix_free(spm);
	return test_success();
}


int server_test_triangle_adjacency1(void) {
	test_start("Triangle m = 2 adjacency");
	gsl_spmatrix *spm1 = generate_graph_adjacency(2, 't');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(24, 24, 60, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 1, 0, one);
	gsl_spmatrix_set(spm2, 1, 4, one);
	gsl_spmatrix_set(spm2, 2, 3, one);
	gsl_spmatrix_set(spm2, 2, 6, one);
	gsl_spmatrix_set(spm2, 3, 0, one);
	gsl_spmatrix_set(spm2, 3, 2, one);
	gsl_spmatrix_set(spm2, 3, 7, one);
	gsl_spmatrix_set(spm2, 4, 1, one);
	gsl_spmatrix_set(spm2, 4, 5, one);
	gsl_spmatrix_set(spm2, 4, 8, one);
	gsl_spmatrix_set(spm2, 5, 4, one);
	gsl_spmatrix_set(spm2, 5, 9, one);
	gsl_spmatrix_set(spm2, 6, 2, one);
	gsl_spmatrix_set(spm2, 6, 10, one);
	gsl_spmatrix_set(spm2, 7, 3, one);
	gsl_spmatrix_set(spm2, 7, 8, one);
	gsl_spmatrix_set(spm2, 7, 11, one);
	gsl_spmatrix_set(spm2, 8, 4, one);
	gsl_spmatrix_set(spm2, 8, 7, one);
	gsl_spmatrix_set(spm2, 8, 12, one);
	gsl_spmatrix_set(spm2, 9, 5, one);
	gsl_spmatrix_set(spm2, 9, 13, one);
	gsl_spmatrix_set(spm2, 10, 6, one);
	gsl_spmatrix_set(spm2, 10, 11, one);
	gsl_spmatrix_set(spm2, 10, 14, one);
	gsl_spmatrix_set(spm2, 11, 7, one);
	gsl_spmatrix_set(spm2, 11, 10, one);
	gsl_spmatrix_set(spm2, 11, 15, one);
	gsl_spmatrix_set(spm2, 12, 8, one);
	gsl_spmatrix_set(spm2, 12, 13, one);
	gsl_spmatrix_set(spm2, 12, 16, one);
	gsl_spmatrix_set(spm2, 13, 9, one);
	gsl_spmatrix_set(spm2, 13, 12, one);
	gsl_spmatrix_set(spm2, 13, 17, one);
	gsl_spmatrix_set(spm2, 14, 10, one);
	gsl_spmatrix_set(spm2, 14, 18, one);
	gsl_spmatrix_set(spm2, 15, 11, one);
	gsl_spmatrix_set(spm2, 15, 16, one);
	gsl_spmatrix_set(spm2, 15, 19, one);
	gsl_spmatrix_set(spm2, 16, 12, one);
	gsl_spmatrix_set(spm2, 16, 15, one);
	gsl_spmatrix_set(spm2, 16, 20, one);
	gsl_spmatrix_set(spm2, 17, 13, one);
	gsl_spmatrix_set(spm2, 17, 21, one);
	gsl_spmatrix_set(spm2, 18, 14, one);
	gsl_spmatrix_set(spm2, 18, 19, one);
	gsl_spmatrix_set(spm2, 19, 15, one);
	gsl_spmatrix_set(spm2, 19, 18, one);
	gsl_spmatrix_set(spm2, 19, 22, one);
	gsl_spmatrix_set(spm2, 20, 16, one);
	gsl_spmatrix_set(spm2, 20, 21, one);
	gsl_spmatrix_set(spm2, 20, 23, one);
	gsl_spmatrix_set(spm2, 21, 17, one);
	gsl_spmatrix_set(spm2, 21, 20, one);
	gsl_spmatrix_set(spm2, 22, 19, one);
	gsl_spmatrix_set(spm2, 22, 23, one);
	gsl_spmatrix_set(spm2, 23, 20, one);
	gsl_spmatrix_set(spm2, 23, 22, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a m = 2 triangle adjacency matrix");
}

int server_test_triangle_adjacency2(void) {
	test_start("Triangle m = 4 adjacency");
	gsl_spmatrix *spm1 = generate_graph_adjacency(4, 't');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(96, 96, 264, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 1, 0, one);
	gsl_spmatrix_set(spm2, 1, 4, one);
	gsl_spmatrix_set(spm2, 2, 7, one);
	gsl_spmatrix_set(spm2, 2, 3, one);
	gsl_spmatrix_set(spm2, 3, 2, one);
	gsl_spmatrix_set(spm2, 3, 8, one);
	gsl_spmatrix_set(spm2, 3, 0, one);
	gsl_spmatrix_set(spm2, 4, 1, one);
	gsl_spmatrix_set(spm2, 4, 9, one);
	gsl_spmatrix_set(spm2, 4, 5, one);
	gsl_spmatrix_set(spm2, 5, 4, one);
	gsl_spmatrix_set(spm2, 5, 10, one);
	gsl_spmatrix_set(spm2, 6, 7, one);
	gsl_spmatrix_set(spm2, 6, 13, one);
	gsl_spmatrix_set(spm2, 7, 6, one);
	gsl_spmatrix_set(spm2, 7, 2, one);
	gsl_spmatrix_set(spm2, 7, 14, one);
	gsl_spmatrix_set(spm2, 8, 3, one);
	gsl_spmatrix_set(spm2, 8, 9, one);
	gsl_spmatrix_set(spm2, 8, 15, one);
	gsl_spmatrix_set(spm2, 9, 4, one);
	gsl_spmatrix_set(spm2, 9, 8, one);
	gsl_spmatrix_set(spm2, 9, 16, one);
	gsl_spmatrix_set(spm2, 10, 5, one);
	gsl_spmatrix_set(spm2, 10, 11, one);
	gsl_spmatrix_set(spm2, 10, 17, one);
	gsl_spmatrix_set(spm2, 11, 10, one);
	gsl_spmatrix_set(spm2, 11, 18, one);
	gsl_spmatrix_set(spm2, 12, 20, one);
	gsl_spmatrix_set(spm2, 12, 13, one);
	gsl_spmatrix_set(spm2, 13, 12, one);
	gsl_spmatrix_set(spm2, 13, 6, one);
	gsl_spmatrix_set(spm2, 13, 21, one);
	gsl_spmatrix_set(spm2, 14, 7, one);
	gsl_spmatrix_set(spm2, 14, 22, one);
	gsl_spmatrix_set(spm2, 14, 15, one);
	gsl_spmatrix_set(spm2, 15, 14, one);
	gsl_spmatrix_set(spm2, 15, 8, one);
	gsl_spmatrix_set(spm2, 15, 23, one);
	gsl_spmatrix_set(spm2, 16, 9, one);
	gsl_spmatrix_set(spm2, 16, 17, one);
	gsl_spmatrix_set(spm2, 16, 24, one);
	gsl_spmatrix_set(spm2, 17, 10, one);
	gsl_spmatrix_set(spm2, 17, 16, one);
	gsl_spmatrix_set(spm2, 17, 25, one);
	gsl_spmatrix_set(spm2, 18, 11, one);
	gsl_spmatrix_set(spm2, 18, 19, one);
	gsl_spmatrix_set(spm2, 18, 26, one);
	gsl_spmatrix_set(spm2, 19, 18, one);
	gsl_spmatrix_set(spm2, 19, 27, one);
	gsl_spmatrix_set(spm2, 20, 12, one);
	gsl_spmatrix_set(spm2, 20, 28, one);
	gsl_spmatrix_set(spm2, 21, 13, one);
	gsl_spmatrix_set(spm2, 21, 22, one);
	gsl_spmatrix_set(spm2, 21, 29, one);
	gsl_spmatrix_set(spm2, 22, 14, one);
	gsl_spmatrix_set(spm2, 22, 21, one);
	gsl_spmatrix_set(spm2, 22, 30, one);
	gsl_spmatrix_set(spm2, 23, 15, one);
	gsl_spmatrix_set(spm2, 23, 24, one);
	gsl_spmatrix_set(spm2, 23, 31, one);
	gsl_spmatrix_set(spm2, 24, 16, one);
	gsl_spmatrix_set(spm2, 24, 23, one);
	gsl_spmatrix_set(spm2, 24, 32, one);
	gsl_spmatrix_set(spm2, 25, 17, one);
	gsl_spmatrix_set(spm2, 25, 26, one);
	gsl_spmatrix_set(spm2, 25, 33, one);
	gsl_spmatrix_set(spm2, 26, 18, one);
	gsl_spmatrix_set(spm2, 26, 25, one);
	gsl_spmatrix_set(spm2, 26, 34, one);
	gsl_spmatrix_set(spm2, 27, 19, one);
	gsl_spmatrix_set(spm2, 27, 35, one);
	gsl_spmatrix_set(spm2, 28, 20, one);
	gsl_spmatrix_set(spm2, 28, 29, one);
	gsl_spmatrix_set(spm2, 28, 36, one);
	gsl_spmatrix_set(spm2, 29, 21, one);
	gsl_spmatrix_set(spm2, 29, 28, one);
	gsl_spmatrix_set(spm2, 29, 37, one);
	gsl_spmatrix_set(spm2, 30, 31, one);
	gsl_spmatrix_set(spm2, 30, 22, one);
	gsl_spmatrix_set(spm2, 30, 38, one);
	gsl_spmatrix_set(spm2, 31, 23, one);
	gsl_spmatrix_set(spm2, 31, 30, one);
	gsl_spmatrix_set(spm2, 31, 39, one);
	gsl_spmatrix_set(spm2, 32, 24, one);
	gsl_spmatrix_set(spm2, 32, 33, one);
	gsl_spmatrix_set(spm2, 32, 40, one);
	gsl_spmatrix_set(spm2, 33, 25, one);
	gsl_spmatrix_set(spm2, 33, 32, one);
	gsl_spmatrix_set(spm2, 33, 41, one);
	gsl_spmatrix_set(spm2, 34, 26, one);
	gsl_spmatrix_set(spm2, 34, 35, one);
	gsl_spmatrix_set(spm2, 34, 42, one);
	gsl_spmatrix_set(spm2, 35, 27, one);
	gsl_spmatrix_set(spm2, 35, 34, one);
	gsl_spmatrix_set(spm2, 35, 43, one);
	gsl_spmatrix_set(spm2, 36, 28, one);
	gsl_spmatrix_set(spm2, 36, 44, one);
	gsl_spmatrix_set(spm2, 37, 29, one);
	gsl_spmatrix_set(spm2, 37, 38, one);
	gsl_spmatrix_set(spm2, 37, 45, one);
	gsl_spmatrix_set(spm2, 38, 30, one);
	gsl_spmatrix_set(spm2, 38, 37, one);
	gsl_spmatrix_set(spm2, 38, 46, one);
	gsl_spmatrix_set(spm2, 39, 31, one);
	gsl_spmatrix_set(spm2, 39, 40, one);
	gsl_spmatrix_set(spm2, 39, 47, one);
	gsl_spmatrix_set(spm2, 40, 32, one);
	gsl_spmatrix_set(spm2, 40, 39, one);
	gsl_spmatrix_set(spm2, 40, 48, one);
	gsl_spmatrix_set(spm2, 41, 33, one);
	gsl_spmatrix_set(spm2, 41, 42, one);
	gsl_spmatrix_set(spm2, 41, 49, one);
	gsl_spmatrix_set(spm2, 42, 34, one);
	gsl_spmatrix_set(spm2, 42, 41, one);
	gsl_spmatrix_set(spm2, 42, 50, one);
	gsl_spmatrix_set(spm2, 43, 35, one);
	gsl_spmatrix_set(spm2, 43, 51, one);
	gsl_spmatrix_set(spm2, 44, 36, one);
	gsl_spmatrix_set(spm2, 44, 45, one);
	gsl_spmatrix_set(spm2, 44, 52, one);
	gsl_spmatrix_set(spm2, 45, 37, one);
	gsl_spmatrix_set(spm2, 45, 44, one);
	gsl_spmatrix_set(spm2, 45, 53, one);
	gsl_spmatrix_set(spm2, 46, 38, one);
	gsl_spmatrix_set(spm2, 46, 47, one);
	gsl_spmatrix_set(spm2, 46, 54, one);
	gsl_spmatrix_set(spm2, 47, 39, one);
	gsl_spmatrix_set(spm2, 47, 46, one);
	gsl_spmatrix_set(spm2, 47, 55, one);
	gsl_spmatrix_set(spm2, 48, 40, one);
	gsl_spmatrix_set(spm2, 48, 49, one);
	gsl_spmatrix_set(spm2, 48, 56, one);
	gsl_spmatrix_set(spm2, 49, 41, one);
	gsl_spmatrix_set(spm2, 49, 48, one);
	gsl_spmatrix_set(spm2, 49, 57, one);
	gsl_spmatrix_set(spm2, 50, 42, one);
	gsl_spmatrix_set(spm2, 50, 51, one);
	gsl_spmatrix_set(spm2, 50, 58, one);
	gsl_spmatrix_set(spm2, 51, 43, one);
	gsl_spmatrix_set(spm2, 51, 50, one);
	gsl_spmatrix_set(spm2, 51, 59, one);
	gsl_spmatrix_set(spm2, 52, 44, one);
	gsl_spmatrix_set(spm2, 52, 60, one);
	gsl_spmatrix_set(spm2, 53, 45, one);
	gsl_spmatrix_set(spm2, 53, 54, one);
	gsl_spmatrix_set(spm2, 53, 61, one);
	gsl_spmatrix_set(spm2, 54, 46, one);
	gsl_spmatrix_set(spm2, 54, 53, one);
	gsl_spmatrix_set(spm2, 54, 62, one);
	gsl_spmatrix_set(spm2, 55, 47, one);
	gsl_spmatrix_set(spm2, 55, 56, one);
	gsl_spmatrix_set(spm2, 55, 63, one);
	gsl_spmatrix_set(spm2, 56, 48, one);
	gsl_spmatrix_set(spm2, 56, 55, one);
	gsl_spmatrix_set(spm2, 56, 64, one);
	gsl_spmatrix_set(spm2, 57, 49, one);
	gsl_spmatrix_set(spm2, 57, 58, one);
	gsl_spmatrix_set(spm2, 57, 65, one);
	gsl_spmatrix_set(spm2, 58, 50, one);
	gsl_spmatrix_set(spm2, 58, 57, one);
	gsl_spmatrix_set(spm2, 58, 66, one);
	gsl_spmatrix_set(spm2, 59, 51, one);
	gsl_spmatrix_set(spm2, 59, 67, one);
	gsl_spmatrix_set(spm2, 60, 52, one);
	gsl_spmatrix_set(spm2, 60, 61, one);
	gsl_spmatrix_set(spm2, 60, 68, one);
	gsl_spmatrix_set(spm2, 61, 53, one);
	gsl_spmatrix_set(spm2, 61, 60, one);
	gsl_spmatrix_set(spm2, 61, 69, one);
	gsl_spmatrix_set(spm2, 62, 54, one);
	gsl_spmatrix_set(spm2, 62, 63, one);
	gsl_spmatrix_set(spm2, 62, 70, one);
	gsl_spmatrix_set(spm2, 63, 55, one);
	gsl_spmatrix_set(spm2, 63, 62, one);
	gsl_spmatrix_set(spm2, 63, 71, one);
	gsl_spmatrix_set(spm2, 64, 56, one);
	gsl_spmatrix_set(spm2, 64, 65, one);
	gsl_spmatrix_set(spm2, 64, 72, one);
	gsl_spmatrix_set(spm2, 65, 57, one);
	gsl_spmatrix_set(spm2, 65, 64, one);
	gsl_spmatrix_set(spm2, 65, 73, one);
	gsl_spmatrix_set(spm2, 66, 58, one);
	gsl_spmatrix_set(spm2, 66, 67, one);
	gsl_spmatrix_set(spm2, 66, 74, one);
	gsl_spmatrix_set(spm2, 67, 59, one);
	gsl_spmatrix_set(spm2, 67, 66, one);
	gsl_spmatrix_set(spm2, 67, 75, one);
	gsl_spmatrix_set(spm2, 68, 60, one);
	gsl_spmatrix_set(spm2, 68, 76, one);
	gsl_spmatrix_set(spm2, 69, 61, one);
	gsl_spmatrix_set(spm2, 69, 70, one);
	gsl_spmatrix_set(spm2, 69, 77, one);
	gsl_spmatrix_set(spm2, 70, 62, one);
	gsl_spmatrix_set(spm2, 70, 69, one);
	gsl_spmatrix_set(spm2, 70, 78, one);
	gsl_spmatrix_set(spm2, 71, 63, one);
	gsl_spmatrix_set(spm2, 71, 72, one);
	gsl_spmatrix_set(spm2, 71, 79, one);
	gsl_spmatrix_set(spm2, 72, 64, one);
	gsl_spmatrix_set(spm2, 72, 71, one);
	gsl_spmatrix_set(spm2, 72, 80, one);
	gsl_spmatrix_set(spm2, 73, 65, one);
	gsl_spmatrix_set(spm2, 73, 74, one);
	gsl_spmatrix_set(spm2, 73, 81, one);
	gsl_spmatrix_set(spm2, 74, 66, one);
	gsl_spmatrix_set(spm2, 74, 73, one);
	gsl_spmatrix_set(spm2, 74, 82, one);
	gsl_spmatrix_set(spm2, 75, 67, one);
	gsl_spmatrix_set(spm2, 75, 83, one);
	gsl_spmatrix_set(spm2, 76, 68, one);
	gsl_spmatrix_set(spm2, 76, 77, one);
	gsl_spmatrix_set(spm2, 77, 69, one);
	gsl_spmatrix_set(spm2, 77, 76, one);
	gsl_spmatrix_set(spm2, 77, 84, one);
	gsl_spmatrix_set(spm2, 78, 70, one);
	gsl_spmatrix_set(spm2, 78, 79, one);
	gsl_spmatrix_set(spm2, 78, 85, one);
	gsl_spmatrix_set(spm2, 79, 71, one);
	gsl_spmatrix_set(spm2, 79, 78, one);
	gsl_spmatrix_set(spm2, 79, 86, one);
	gsl_spmatrix_set(spm2, 80, 72, one);
	gsl_spmatrix_set(spm2, 80, 81, one);
	gsl_spmatrix_set(spm2, 80, 87, one);
	gsl_spmatrix_set(spm2, 81, 73, one);
	gsl_spmatrix_set(spm2, 81, 80, one);
	gsl_spmatrix_set(spm2, 81, 88, one);
	gsl_spmatrix_set(spm2, 82, 74, one);
	gsl_spmatrix_set(spm2, 82, 83, one);
	gsl_spmatrix_set(spm2, 82, 89, one);
	gsl_spmatrix_set(spm2, 83, 75, one);
	gsl_spmatrix_set(spm2, 83, 82, one);
	gsl_spmatrix_set(spm2, 84, 85, one);
	gsl_spmatrix_set(spm2, 84, 77, one);
	gsl_spmatrix_set(spm2, 85, 78, one);
	gsl_spmatrix_set(spm2, 85, 84, one);
	gsl_spmatrix_set(spm2, 85, 90, one);
	gsl_spmatrix_set(spm2, 86, 79, one);
	gsl_spmatrix_set(spm2, 86, 87, one);
	gsl_spmatrix_set(spm2, 86, 91, one);
	gsl_spmatrix_set(spm2, 87, 80, one);
	gsl_spmatrix_set(spm2, 87, 86, one);
	gsl_spmatrix_set(spm2, 87, 92, one);
	gsl_spmatrix_set(spm2, 88, 81, one);
	gsl_spmatrix_set(spm2, 88, 89, one);
	gsl_spmatrix_set(spm2, 88, 93, one);
	gsl_spmatrix_set(spm2, 89, 82, one);
	gsl_spmatrix_set(spm2, 89, 88, one);
	gsl_spmatrix_set(spm2, 90, 85, one);
	gsl_spmatrix_set(spm2, 90, 91, one);
	gsl_spmatrix_set(spm2, 91, 86, one);
	gsl_spmatrix_set(spm2, 91, 90, one);
	gsl_spmatrix_set(spm2, 91, 94, one);
	gsl_spmatrix_set(spm2, 92, 87, one);
	gsl_spmatrix_set(spm2, 92, 93, one);
	gsl_spmatrix_set(spm2, 92, 95, one);
	gsl_spmatrix_set(spm2, 93, 88, one);
	gsl_spmatrix_set(spm2, 93, 92, one);
	gsl_spmatrix_set(spm2, 94, 91, one);
	gsl_spmatrix_set(spm2, 94, 95, one);
	gsl_spmatrix_set(spm2, 95, 92, one);
	gsl_spmatrix_set(spm2, 95, 94, one);
	for (int i = 0; i < 96; i++) {
		for (int j = 0; j < 96; j++) {
			if (gsl_spmatrix_get(spm1, i, j) != gsl_spmatrix_get(spm2, i, j))
				printf("%lf %d %d", gsl_spmatrix_get(spm1, i, j), i, j);
		}
	}
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a m = 4 triangle adjacency matrix");
}

int server_test_square_nb_vertices(void) {
	test_start("Square nb vertices");
	size_t nb_vertices1 = 9;
	size_t nb_vertices2 = generate_nb_vertices(3, 'c');
	if (nb_vertices1 == nb_vertices2)
		return test_success();
	return test_failure("Nb_vertices square generator failed");
}

int server_test_hex_nb_vertices(void) {
	test_start("Hex nb vertices");
	size_t nb_vertices1 = 9;
	size_t nb_vertices2 = generate_nb_vertices(3, 'h');
	if (nb_vertices1 == nb_vertices2)
		return test_success();
	return test_failure("Nb_vertices hex generator failed");
}

int server_test_triangle_nb_vertices(void) {
	test_start("Triangle nb vertices");
	size_t nb_vertices1 = 24;
	size_t nb_vertices2 = generate_nb_vertices(2, 't');
	if (nb_vertices1 == nb_vertices2)
		return test_success();
	return test_failure("Nb_vertices triangle generator failed");
}

int server_test_square_ownership1(void) {
	test_start("Square m = 3 ownership");
	gsl_spmatrix *spm1 = generate_graph_ownership(3, 'c');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(2, 9, 8, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 0, one);
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 7, one);
	gsl_spmatrix_set(spm2, 0, 8, one);
	gsl_spmatrix_set(spm2, 1, 3, one);
	gsl_spmatrix_set(spm2, 1, 6, one);
	gsl_spmatrix_set(spm2, 1, 2, one);
	gsl_spmatrix_set(spm2, 1, 5, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a 3*3 square ownership matrix");
}

int server_test_square_ownership2(void) {
	test_start("Square m = 5 ownership");
	gsl_spmatrix *spm1 = generate_graph_ownership(5, 'c');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(2, 25, 16, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 0, one);
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 2, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 0, 21, one);
	gsl_spmatrix_set(spm2, 0, 22, one);
	gsl_spmatrix_set(spm2, 0, 23, one);
	gsl_spmatrix_set(spm2, 0, 24, one);
	gsl_spmatrix_set(spm2, 1, 4, one);
	gsl_spmatrix_set(spm2, 1, 5, one);
	gsl_spmatrix_set(spm2, 1, 9, one);
	gsl_spmatrix_set(spm2, 1, 10, one);
	gsl_spmatrix_set(spm2, 1, 14, one);
	gsl_spmatrix_set(spm2, 1, 15, one);
	gsl_spmatrix_set(spm2, 1, 19, one);
	gsl_spmatrix_set(spm2, 1, 20, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a 5*5 square ownership matrix");
}

int server_test_hex_ownership(void) {
	test_start("Hex ownership");
	gsl_spmatrix *spm1 = generate_graph_ownership(3, 'h');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(2, 9, 8, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 0, one);
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 7, one);
	gsl_spmatrix_set(spm2, 0, 8, one);
	gsl_spmatrix_set(spm2, 1, 3, one);
	gsl_spmatrix_set(spm2, 1, 6, one);
	gsl_spmatrix_set(spm2, 1, 2, one);
	gsl_spmatrix_set(spm2, 1, 5, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a 3*3 hex ownership matrix");
}

int server_test_triangle_ownership1(void) {
	test_start("Triangle m = 2 ownership");
	gsl_spmatrix *spm1 = generate_graph_ownership(2, 't');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(2, 24, 16, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 2, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 0, 0, one);
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 22, one);
	gsl_spmatrix_set(spm2, 0, 23, one);
	gsl_spmatrix_set(spm2, 0, 20, one);
	gsl_spmatrix_set(spm2, 0, 21, one);
	gsl_spmatrix_set(spm2, 1, 6, one);
	gsl_spmatrix_set(spm2, 1, 10, one);
	gsl_spmatrix_set(spm2, 1, 14, one);
	gsl_spmatrix_set(spm2, 1, 18, one);
	gsl_spmatrix_set(spm2, 1, 5, one);
	gsl_spmatrix_set(spm2, 1, 9, one);
	gsl_spmatrix_set(spm2, 1, 13, one);
	gsl_spmatrix_set(spm2, 1, 17, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a m = 2 triangle ownership matrix");
}


int server_test_triangle_ownership2(void) {
	test_start("Triangle m = 4 ownership");
	gsl_spmatrix *spm1 = generate_graph_ownership(4, 't');
	gsl_spmatrix *spm2 = gsl_spmatrix_alloc_nzmax(2, 96, 40, GSL_SPMATRIX_COO);
	gsl_spmatrix_set_zero(spm2);
	double one = 1;
	gsl_spmatrix_set(spm2, 0, 12, one);
	gsl_spmatrix_set(spm2, 0, 13, one);
	gsl_spmatrix_set(spm2, 0, 6, one);
	gsl_spmatrix_set(spm2, 0, 7, one);
	gsl_spmatrix_set(spm2, 0, 2, one);
	gsl_spmatrix_set(spm2, 0, 3, one);
	gsl_spmatrix_set(spm2, 0, 0, one);
	gsl_spmatrix_set(spm2, 0, 1, one);
	gsl_spmatrix_set(spm2, 0, 4, one);
	gsl_spmatrix_set(spm2, 0, 5, one);
	gsl_spmatrix_set(spm2, 0, 90, one);
	gsl_spmatrix_set(spm2, 0, 91, one);
	gsl_spmatrix_set(spm2, 0, 94, one);
	gsl_spmatrix_set(spm2, 0, 95, one);
	gsl_spmatrix_set(spm2, 0, 92, one);
	gsl_spmatrix_set(spm2, 0, 93, one);
	gsl_spmatrix_set(spm2, 0, 88, one);
	gsl_spmatrix_set(spm2, 0, 89, one);
	gsl_spmatrix_set(spm2, 0, 82, one);
	gsl_spmatrix_set(spm2, 0, 83, one);
	gsl_spmatrix_set(spm2, 1, 20, one);
	gsl_spmatrix_set(spm2, 1, 28, one);
	gsl_spmatrix_set(spm2, 1, 36, one);
	gsl_spmatrix_set(spm2, 1, 44, one);
	gsl_spmatrix_set(spm2, 1, 52, one);
	gsl_spmatrix_set(spm2, 1, 60, one);
	gsl_spmatrix_set(spm2, 1, 68, one);
	gsl_spmatrix_set(spm2, 1, 76, one);
	gsl_spmatrix_set(spm2, 1, 77, one);
	gsl_spmatrix_set(spm2, 1, 84, one);
	gsl_spmatrix_set(spm2, 1, 75, one);
	gsl_spmatrix_set(spm2, 1, 67, one);
	gsl_spmatrix_set(spm2, 1, 59, one);
	gsl_spmatrix_set(spm2, 1, 51, one);
	gsl_spmatrix_set(spm2, 1, 43, one);
	gsl_spmatrix_set(spm2, 1, 35, one);
	gsl_spmatrix_set(spm2, 1, 27, one);
	gsl_spmatrix_set(spm2, 1, 19, one);
	gsl_spmatrix_set(spm2, 1, 18, one);
	gsl_spmatrix_set(spm2, 1, 11, one);
	if (gsl_spmatrix_equal(spm1, spm2)) {
		gsl_spmatrix_free(spm1);
		gsl_spmatrix_free(spm2);
		return test_success();
	}
	gsl_spmatrix_free(spm1);
	gsl_spmatrix_free(spm2);
	return test_failure("Generator fails to give a m = 4 triangle ownership matrix");
}

int load_strategy_tests() {
	test_start("load_strategy");

	struct strategy *s = load_strategy("cmake-build/src/random/librandom.so");
	if (s) {
		printf("Loaded strategy: %s ", s->get_player_name());
		free_strategy(s);
		return test_success();
	} else {
		char *dlerror_message = dlerror();
		return test_failure(dlerror_message ? dlerror_message : "Could not find all symbols.");
	}
}


int free_graph_test() {
	test_start("free_graph");

	struct graph_t *g = generate_empty_graph(4, 'h');
	free_graph(g);
	return test_success();
}

int server_tests() {
	test_suite_start("server");

	int nbr_failures = 0;

	nbr_failures += test_local_path();
	nbr_failures += server_test_square_adjacency();
	nbr_failures += server_test_hex_adjacency();
	nbr_failures += server_test_hex_adjacency_incomplete();
	nbr_failures += server_test_triangle_adjacency1();
	nbr_failures += server_test_triangle_adjacency2();
	nbr_failures += server_test_square_nb_vertices();
	nbr_failures += server_test_hex_nb_vertices();
	nbr_failures += server_test_triangle_nb_vertices();
	nbr_failures += server_test_square_ownership1();
	nbr_failures += server_test_square_ownership2();
	nbr_failures += server_test_hex_ownership();
	nbr_failures += server_test_triangle_ownership1();
	nbr_failures += server_test_triangle_ownership2();
	nbr_failures += load_strategy_tests();
	nbr_failures += free_graph_test();

	return nbr_failures;
}
