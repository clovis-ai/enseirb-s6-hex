
# Code of the strategy 'random'

set(RANDOM
    main/main.c
    main/random_player.c)

# Creation of 'random.so'

add_library(random SHARED ${RANDOM})
target_link_libraries(random common)
apply_gsl(random)

# Tests of the strategy 'random'

add_library(random-tests STATIC
            test/random_test.c
            test/random_test.h
            ${RANDOM})

target_compile_options(random-tests PRIVATE "-fPIC")
apply_gsl(random-tests)
