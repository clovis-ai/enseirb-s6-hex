#include <stdio.h>
#include <string.h>
#include <graph.h>
#include <move.h>
#include <test_suite.h>
#include <player.h>
#include <stdlib.h>
#include <gsl/gsl_spmatrix.h>
#include <common.h>

int random_test_name(void) {
	test_start("name");

	if (strcmp("random strategy by 9154", get_player_name()))
		return test_failure("Not the right name");

	return test_success();
}


int random_test_propose_opening_different(void) {
	test_start("propose opening randomly");

	struct move_t move1 = propose_opening();
	struct move_t move2 = propose_opening();
	struct move_t move3 = propose_opening();
	struct move_t move4 = propose_opening();

	if (move1.m == move2.m && move2.m == move3.m && move3.m == move4.m)
		return test_failure("Return always the same opening value");

	return test_success();
}

int random_test_propose_opening_correct(void) {
	test_start("propose correct opening");

	struct move_t move = propose_opening();

	if (move.c == 2)
		return test_failure("Incorrect color");

	return test_success();
}

int random_test_propose_opening_correct2(void) {
	test_start("propose correct opening 2");

	struct graph_t *graph = malloc(sizeof(struct graph_t));

	size_t k = 2;
	graph->num_vertices = k;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->t = NULL;

	initialize_graph(graph);
	struct move_t move = propose_opening();

	if (move.m != 1 && move.m != 0)
		return test_failure("Propose an occupied place");

	finalize();

	return test_success();
}

int random_test_accept_opening(void) {
	test_start("accept opening");

	struct graph_t *graph = malloc(sizeof(struct graph_t));

	size_t k = 2;
	graph->num_vertices = k;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->t = NULL;

	initialize_graph(graph);

	struct move_t move = {0, 1};

	if (accept_opening(move)) {
		finalize();
		return test_failure("Accept opening instead of refuse");
	}

	finalize();
	return test_success();
}


int random_test_play_different(void) {
	test_start("play randomly");

	struct graph_t *graph = malloc(sizeof(struct graph_t));
	graph->o = gsl_spmatrix_alloc(2, 9);
	graph->num_vertices = 9;
	graph->t = NULL;

	initialize_graph(graph);

	struct move_t move1 = {0, 1};
	struct move_t move2 = {0, 0};
	struct move_t move3 = {0, 1};
	struct move_t move4 = {0, 1};

	move1 = play(move1);
	move2 = play(move1);
	move3 = play(move1);
	move4 = play(move1);


	if (move1.m == move2.m && move2.m == move3.m && move3.m == move4.m) {
		finalize();
		return test_failure("Return always the same value");
	}

	finalize();

	return test_success();
}

int random_test_play_inside_the_board(void) {
	test_start("play inside the board");

	struct move_t move = {0, 2};
	struct graph_t *graph = malloc(sizeof(struct graph_t));

	size_t k = 2;
	graph->num_vertices = k;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->t = NULL;

	initialize_graph(graph);
	move = play(move);

	if (move.m >= k) {
		finalize();
		return test_failure("Play outside the board");
	}
	finalize();
	return test_success();
}

int random_play_free_place(void) {
	test_start("play free place");

	struct move_t move = {0, 2};
	struct graph_t *graph = malloc(sizeof(struct graph_t));

	size_t k = 2;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->num_vertices = k;
	gsl_spmatrix_set(graph->o, 0, 1, 1);
	graph->t = NULL;

	initialize_graph(graph);
	move = play(move);

	finalize();

	if (move.m == 1)
		return test_failure("Try to play occupied place");

	return test_success();
}

int random_play_free_place_random_several(void) {
	test_start("play free place random multiples times");

	struct move_t move = {0, 2};
	struct graph_t *graph;

	size_t k = 6;
	size_t p1, p2, p3;

	for (size_t i = 0; i < (k * k) / 3; i++) {
		graph = malloc(sizeof(struct graph_t));
		graph->o = gsl_spmatrix_alloc(2, k);
		graph->num_vertices = k;
		graph->t = NULL;

		p1 = rand() % k;
		p2 = rand() % k;
		p3 = rand() % k;

		gsl_spmatrix_set(graph->o, 0, p1, 1);
		gsl_spmatrix_set(graph->o, 1, p2, 1);
		gsl_spmatrix_set(graph->o, 1, p3, 1);

		initialize_graph(graph);
		move = play(move);

		finalize();

		if (move.m == p1 || move.m == p2 || move.m == p3)
			return test_failure("Try to play occupied place");
	}

	return test_success();
}

int random_play_memory_update(void) {
	test_start("update it memory");

	struct move_t move = {0, 2};
	struct graph_t *graph = malloc(sizeof(struct graph_t));
	struct graph_t graph_ref;
	graph->t = NULL;

	size_t k = 10;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph_ref.o = gsl_spmatrix_alloc(2, k);

	graph->num_vertices = k;
	graph_ref.num_vertices = k;

	size_t first_place = 0;

	gsl_spmatrix_memcpy(graph->o, graph_ref.o);

	gsl_spmatrix_set(graph_ref.o, 0, first_place, 1);

	move.m = first_place;
	move.c = 1;

	initialize_graph(graph);
	initialize_color(1);

	for (size_t i = 0; i < k - 1; i++) {
		move = play(move);

		if (gsl_spmatrix_get(graph_ref.o, 0, move.m) == 1 || gsl_spmatrix_get(graph_ref.o, 1, move.m) == 1) {
			gsl_spmatrix_free(graph_ref.o);
			finalize();
			return test_failure("Play occupied place");
		}
		gsl_spmatrix_set(graph_ref.o, 0, move.m, 1);
	}

	gsl_spmatrix_free(graph_ref.o);
	finalize();
	return test_success();
}

int random_test_update_memory_personal_move(void) {
	test_start("personal move updating memory");

	size_t k = 25;

	size_t tab[k];
	for (int i = 0; i < (int) k; i++) {
		tab[i] = k + 1;
	}

	struct graph_t *graph = malloc(sizeof(struct graph_t));
	graph->num_vertices = k;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->t = NULL;

	initialize_graph(graph);
	initialize_color(1);

	struct move_t move = {0, 0};

	tab[0] = 0;

	for (int i = 1; i < (int) k; i++) {
		move.m = 0;
		move = play(move);
		for (int j = 0; j < (int) k; j++) {
			if (tab[j] == move.m) {
				finalize();
				return test_failure("Play an occupied place");
			}
		}
		tab[i] = move.m;
	}

	finalize();
	return test_success();
}

int random_test_hard(void) {
	test_start("full test");

	size_t k = 42;

	size_t tab[k];
	for (int i = 0; i < (int) k; i++) {
		tab[i] = k + 1;
	}

	struct graph_t *graph = malloc(sizeof(struct graph_t));
	graph->num_vertices = k;
	graph->o = gsl_spmatrix_alloc(2, k);
	graph->t = NULL;

	initialize_graph(graph);
	initialize_color(1);

	struct move_t move = propose_opening();

	tab[0] = move.m;

	accept_opening(move);

	for (int i = 1; i < (int) k; i++) {
		move = play(move);
		for (int j = 0; j < (int) k; j++) {
			if (tab[j] == move.m) {
				finalize();
				return test_failure("Play an occupied place");
			}
		}
		tab[i] = move.m;
	}

	finalize();
	return test_success();
}

int random_tests(void) {
	test_suite_start("random");

	int nbr_failures = 0;

	nbr_failures += random_test_name();
	nbr_failures += random_test_propose_opening_different();
	nbr_failures += random_test_propose_opening_correct();
	nbr_failures += random_test_propose_opening_correct2();
	nbr_failures += random_test_accept_opening();
	nbr_failures += random_test_play_different();
	nbr_failures += random_test_play_inside_the_board();
	nbr_failures += random_play_free_place();
	nbr_failures += random_play_free_place_random_several();
	nbr_failures += random_play_memory_update();
	nbr_failures += random_test_hard();
	nbr_failures += random_test_update_memory_personal_move();

	return nbr_failures;
}

  

