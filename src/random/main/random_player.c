#include <stdlib.h>
#include <stdio.h>
#include <move.h>
#include <graph.h>
#include <gsl/gsl_spmatrix.h>


const char *rNAME = "random strategy by 9154";
size_t rNB_VERTICES = 9;

/**
 * Documentation in common/include/graph.h
 */
gsl_spmatrix *rOWNERSHIP = NULL;
int rCOLOR = 2;

/**
 * Try if the place number m is occupied or not
 * @param m number of the place that would be test
 * @return a boolean telling if teh place is free
 */
static int occupied(size_t m) {
	return (gsl_spmatrix_get(rOWNERSHIP, 0, m) == 1 || gsl_spmatrix_get(rOWNERSHIP, 1, m) == 1);
}

char const *get_player_name() {
	return rNAME;
}

struct move_t propose_opening() {
	size_t m = rand() % rNB_VERTICES;

	if (rOWNERSHIP != NULL) {
		while (occupied(m))
			m = rand() % rNB_VERTICES;
	}

	struct move_t move = {m, 0};

	if (rOWNERSHIP != NULL)
		gsl_spmatrix_set(rOWNERSHIP, 0, m, 1);

	return move;
}

int accept_opening(const struct move_t opening) {
	if (rOWNERSHIP != NULL)
		gsl_spmatrix_set(rOWNERSHIP, 0, opening.m, 1);

	return 0;
}

void initialize_graph(struct graph_t *graph) {
	rNB_VERTICES = graph->num_vertices;
	rOWNERSHIP = graph->o;
	if (graph->t != NULL)
		gsl_spmatrix_free(graph->t);
	free(graph);
}

void initialize_color(enum color_t id) {
	rCOLOR = id;
}

struct move_t play(struct move_t previous_move) {
	if (previous_move.c == 1 || previous_move.c == 0)
		gsl_spmatrix_set(rOWNERSHIP, 0, previous_move.m, 1);

	size_t m = rand() % rNB_VERTICES;

	if (rOWNERSHIP != NULL) {
		while (occupied(m))
			m = rand() % rNB_VERTICES;
	}

	struct move_t move = {m, rCOLOR};
	gsl_spmatrix_set(rOWNERSHIP, 0, m, 1);

	return move;
}

void finalize() {
	gsl_spmatrix_free(rOWNERSHIP);
}
